//
//  Colly.h
//  Colly
//
//  Created by Ales Tsurko on 15.10.16.
//  Copyright © 2016 Aliaksandr Tsurko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Colly.
FOUNDATION_EXPORT double CollyVersionNumber;

//! Project version string for Colly.
FOUNDATION_EXPORT const unsigned char CollyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Colly/PublicHeader.h>


