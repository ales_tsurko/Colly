//
//  Symbol.swift
//  Colly
//
//  Created by Ales Tsurko on 12/6/16.
//  Copyright © 2016 Aliaksandr Tsurko. All rights reserved.
//

import Foundation

enum SymbolType {
	/* TODO: write all possibly types here (defined in the grammar) */
	case
		Variable,
		Number,
		String,
		Array,
		Dictionary,
		Range,
		Bar,
		Pattern,
		Closure
}

public class Symbol {
	let name: String
	let type: SymbolType
	
	init(name: String, type: SymbolType) {
		self.name = name
		self.type = type
	}
}
