//
//  SymbolTable.swift
//  Colly
//
//  Created by Ales Tsurko on 12/6/16.
//  Copyright © 2016 Aliaksandr Tsurko. All rights reserved.
//

import Foundation

public class SymbolTable {
	let scopeName = "global"
	var parentScope: SymbolTable?
	var symbols = [String: Symbol]()
	
	func define(symbol: Symbol) {
		self.symbols[symbol.name] = symbol
	}
	
	func lookup(name: String) -> Symbol? {
		return symbols[name]
	}
}
