//
//  Colly.swift
//  Colly
//
//  Created by Ales Tsurko on 12/6/16.
//  Copyright © 2016 Aliaksandr Tsurko. All rights reserved.
//

import Foundation
import Antlr4

open class Colly: CollyParserBaseListener {
	var numbersStack: [Double] = []
	let globalScope: SymbolTable
	var currentScope: SymbolTable!
	
	override public init() {
		self.globalScope = SymbolTable()
	}
	
	override open func enterScore(_ ctx: CollyParser.ScoreContext) {
		self.currentScope = self.globalScope
	}
	
	override open func exitExpressionAssignment(_ ctx: CollyParser.ExpressionAssignmentContext) {
		
		let id = ctx.ID()!.getText()
		let value = numbersStack.removeLast()
		
		let symbol = Symbol(name: id, type: SymbolType.Variable)
		// блин, фигня получается
		// epxression может быть что угодно здесь
		// от числа со строкой, до паттерна
		// то есть использовать стек как сейчас —- вообще бредово
		// если бы можно было здесь уже сразу иметь значение...
		// Например, тот же получаю выражение, делаю его парсинг и
		// назначаю назначаю уже результат... это похоже на Parse combinator
		// те библы, что я нашел для C++
		// Хотя там возникает такая же проблема
		// Нужно просто найти примеры, как это сделано
		currentScope.define(symbol: <#T##Symbol#>)
	}
	
	open override func exitNumberCall(_ ctx: CollyParser.NumberCallContext) {
		
	}
}
