// Generated from CollyParser.g4 by ANTLR 4.5.3
import Antlr4

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CollyParser}.
 */
public protocol CollyParserListener: ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CollyParser#score}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterScore(_ ctx: CollyParser.ScoreContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#score}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitScore(_ ctx: CollyParser.ScoreContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#end_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterEnd_stat(_ ctx: CollyParser.End_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#end_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitEnd_stat(_ ctx: CollyParser.End_statContext)
	/**
	 * Enter a parse tree produced by the {@code expressionStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterExpressionStatement(_ ctx: CollyParser.ExpressionStatementContext)
	/**
	 * Exit a parse tree produced by the {@code expressionStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitExpressionStatement(_ ctx: CollyParser.ExpressionStatementContext)
	/**
	 * Enter a parse tree produced by the {@code assignStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterAssignStatement(_ ctx: CollyParser.AssignStatementContext)
	/**
	 * Exit a parse tree produced by the {@code assignStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitAssignStatement(_ ctx: CollyParser.AssignStatementContext)
	/**
	 * Enter a parse tree produced by the {@code functionDefinition}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterFunctionDefinition(_ ctx: CollyParser.FunctionDefinitionContext)
	/**
	 * Exit a parse tree produced by the {@code functionDefinition}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitFunctionDefinition(_ ctx: CollyParser.FunctionDefinitionContext)
	/**
	 * Enter a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterIfStatement(_ ctx: CollyParser.IfStatementContext)
	/**
	 * Exit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitIfStatement(_ ctx: CollyParser.IfStatementContext)
	/**
	 * Enter a parse tree produced by the {@code switchStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSwitchStatement(_ ctx: CollyParser.SwitchStatementContext)
	/**
	 * Exit a parse tree produced by the {@code switchStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSwitchStatement(_ ctx: CollyParser.SwitchStatementContext)
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterWhileStatement(_ ctx: CollyParser.WhileStatementContext)
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitWhileStatement(_ ctx: CollyParser.WhileStatementContext)
	/**
	 * Enter a parse tree produced by the {@code doUntilStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterDoUntilStatement(_ ctx: CollyParser.DoUntilStatementContext)
	/**
	 * Exit a parse tree produced by the {@code doUntilStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitDoUntilStatement(_ ctx: CollyParser.DoUntilStatementContext)
	/**
	 * Enter a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterReturnStatement(_ ctx: CollyParser.ReturnStatementContext)
	/**
	 * Exit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitReturnStatement(_ ctx: CollyParser.ReturnStatementContext)
	/**
	 * Enter a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBreakStatement(_ ctx: CollyParser.BreakStatementContext)
	/**
	 * Exit a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBreakStatement(_ ctx: CollyParser.BreakStatementContext)
	/**
	 * Enter a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterContinueStatement(_ ctx: CollyParser.ContinueStatementContext)
	/**
	 * Exit a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link CollyParser#stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitContinueStatement(_ ctx: CollyParser.ContinueStatementContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#return_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterReturn_stat(_ ctx: CollyParser.Return_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#return_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitReturn_stat(_ ctx: CollyParser.Return_statContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#func_def}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterFunc_def(_ ctx: CollyParser.Func_defContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#func_def}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitFunc_def(_ ctx: CollyParser.Func_defContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#func_params}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterFunc_params(_ ctx: CollyParser.Func_paramsContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#func_params}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitFunc_params(_ ctx: CollyParser.Func_paramsContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#if_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterIf_stat(_ ctx: CollyParser.If_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#if_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitIf_stat(_ ctx: CollyParser.If_statContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#else_if}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterElse_if(_ ctx: CollyParser.Else_ifContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#else_if}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitElse_if(_ ctx: CollyParser.Else_ifContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#if_else}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterIf_else(_ ctx: CollyParser.If_elseContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#if_else}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitIf_else(_ ctx: CollyParser.If_elseContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#switch_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSwitch_stat(_ ctx: CollyParser.Switch_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#switch_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSwitch_stat(_ ctx: CollyParser.Switch_statContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#switch_case}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSwitch_case(_ ctx: CollyParser.Switch_caseContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#switch_case}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSwitch_case(_ ctx: CollyParser.Switch_caseContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#switch_default}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSwitch_default(_ ctx: CollyParser.Switch_defaultContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#switch_default}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSwitch_default(_ ctx: CollyParser.Switch_defaultContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#while_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterWhile_stat(_ ctx: CollyParser.While_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#while_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitWhile_stat(_ ctx: CollyParser.While_statContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#do_until_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterDo_until_stat(_ ctx: CollyParser.Do_until_statContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#do_until_stat}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitDo_until_stat(_ ctx: CollyParser.Do_until_statContext)
	/**
	 * Enter a parse tree produced by the {@code expressionAssignment}
	 * labeled alternative in {@link CollyParser#assignment}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterExpressionAssignment(_ ctx: CollyParser.ExpressionAssignmentContext)
	/**
	 * Exit a parse tree produced by the {@code expressionAssignment}
	 * labeled alternative in {@link CollyParser#assignment}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitExpressionAssignment(_ ctx: CollyParser.ExpressionAssignmentContext)
	/**
	 * Enter a parse tree produced by the {@code patternAssignment}
	 * labeled alternative in {@link CollyParser#assignment}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPatternAssignment(_ ctx: CollyParser.PatternAssignmentContext)
	/**
	 * Exit a parse tree produced by the {@code patternAssignment}
	 * labeled alternative in {@link CollyParser#assignment}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPatternAssignment(_ ctx: CollyParser.PatternAssignmentContext)
	/**
	 * Enter a parse tree produced by the {@code binaryOrExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBinaryOrExpression(_ ctx: CollyParser.BinaryOrExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code binaryOrExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBinaryOrExpression(_ ctx: CollyParser.BinaryOrExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code binaryXorExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBinaryXorExpression(_ ctx: CollyParser.BinaryXorExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code binaryXorExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBinaryXorExpression(_ ctx: CollyParser.BinaryXorExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code unaryPlusMinus}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterUnaryPlusMinus(_ ctx: CollyParser.UnaryPlusMinusContext)
	/**
	 * Exit a parse tree produced by the {@code unaryPlusMinus}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitUnaryPlusMinus(_ ctx: CollyParser.UnaryPlusMinusContext)
	/**
	 * Enter a parse tree produced by the {@code rangeCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterRangeCall(_ ctx: CollyParser.RangeCallContext)
	/**
	 * Exit a parse tree produced by the {@code rangeCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitRangeCall(_ ctx: CollyParser.RangeCallContext)
	/**
	 * Enter a parse tree produced by the {@code numberCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterNumberCall(_ ctx: CollyParser.NumberCallContext)
	/**
	 * Exit a parse tree produced by the {@code numberCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitNumberCall(_ ctx: CollyParser.NumberCallContext)
	/**
	 * Enter a parse tree produced by the {@code idCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterIdCall(_ ctx: CollyParser.IdCallContext)
	/**
	 * Exit a parse tree produced by the {@code idCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitIdCall(_ ctx: CollyParser.IdCallContext)
	/**
	 * Enter a parse tree produced by the {@code binaryAndExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBinaryAndExpression(_ ctx: CollyParser.BinaryAndExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code binaryAndExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBinaryAndExpression(_ ctx: CollyParser.BinaryAndExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code exprThenAssign}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterExprThenAssign(_ ctx: CollyParser.ExprThenAssignContext)
	/**
	 * Exit a parse tree produced by the {@code exprThenAssign}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitExprThenAssign(_ ctx: CollyParser.ExprThenAssignContext)
	/**
	 * Enter a parse tree produced by the {@code dictionaryCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterDictionaryCall(_ ctx: CollyParser.DictionaryCallContext)
	/**
	 * Exit a parse tree produced by the {@code dictionaryCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitDictionaryCall(_ ctx: CollyParser.DictionaryCallContext)
	/**
	 * Enter a parse tree produced by the {@code propertyAssign}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPropertyAssign(_ ctx: CollyParser.PropertyAssignContext)
	/**
	 * Exit a parse tree produced by the {@code propertyAssign}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPropertyAssign(_ ctx: CollyParser.PropertyAssignContext)
	/**
	 * Enter a parse tree produced by the {@code dotCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterDotCall(_ ctx: CollyParser.DotCallContext)
	/**
	 * Exit a parse tree produced by the {@code dotCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitDotCall(_ ctx: CollyParser.DotCallContext)
	/**
	 * Enter a parse tree produced by the {@code arrayCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterArrayCall(_ ctx: CollyParser.ArrayCallContext)
	/**
	 * Exit a parse tree produced by the {@code arrayCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitArrayCall(_ ctx: CollyParser.ArrayCallContext)
	/**
	 * Enter a parse tree produced by the {@code lessGreaterExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterLessGreaterExpression(_ ctx: CollyParser.LessGreaterExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code lessGreaterExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitLessGreaterExpression(_ ctx: CollyParser.LessGreaterExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code mulDivModExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterMulDivModExpression(_ ctx: CollyParser.MulDivModExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code mulDivModExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitMulDivModExpression(_ ctx: CollyParser.MulDivModExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code patternExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPatternExpression(_ ctx: CollyParser.PatternExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code patternExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPatternExpression(_ ctx: CollyParser.PatternExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code binaryShift}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBinaryShift(_ ctx: CollyParser.BinaryShiftContext)
	/**
	 * Exit a parse tree produced by the {@code binaryShift}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBinaryShift(_ ctx: CollyParser.BinaryShiftContext)
	/**
	 * Enter a parse tree produced by the {@code booleanOrExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBooleanOrExpression(_ ctx: CollyParser.BooleanOrExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code booleanOrExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBooleanOrExpression(_ ctx: CollyParser.BooleanOrExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code subscriptCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSubscriptCall(_ ctx: CollyParser.SubscriptCallContext)
	/**
	 * Exit a parse tree produced by the {@code subscriptCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSubscriptCall(_ ctx: CollyParser.SubscriptCallContext)
	/**
	 * Enter a parse tree produced by the {@code logicalAndBinaryNegation}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterLogicalAndBinaryNegation(_ ctx: CollyParser.LogicalAndBinaryNegationContext)
	/**
	 * Exit a parse tree produced by the {@code logicalAndBinaryNegation}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitLogicalAndBinaryNegation(_ ctx: CollyParser.LogicalAndBinaryNegationContext)
	/**
	 * Enter a parse tree produced by the {@code equalityExpsression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterEqualityExpsression(_ ctx: CollyParser.EqualityExpsressionContext)
	/**
	 * Exit a parse tree produced by the {@code equalityExpsression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitEqualityExpsression(_ ctx: CollyParser.EqualityExpsressionContext)
	/**
	 * Enter a parse tree produced by the {@code functionCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterFunctionCall(_ ctx: CollyParser.FunctionCallContext)
	/**
	 * Exit a parse tree produced by the {@code functionCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitFunctionCall(_ ctx: CollyParser.FunctionCallContext)
	/**
	 * Enter a parse tree produced by the {@code closureExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterClosureExpression(_ ctx: CollyParser.ClosureExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code closureExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitClosureExpression(_ ctx: CollyParser.ClosureExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code sumExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterSumExpression(_ ctx: CollyParser.SumExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code sumExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitSumExpression(_ ctx: CollyParser.SumExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code stringCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterStringCall(_ ctx: CollyParser.StringCallContext)
	/**
	 * Exit a parse tree produced by the {@code stringCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitStringCall(_ ctx: CollyParser.StringCallContext)
	/**
	 * Enter a parse tree produced by the {@code parenthesisedExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterParenthesisedExpression(_ ctx: CollyParser.ParenthesisedExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code parenthesisedExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitParenthesisedExpression(_ ctx: CollyParser.ParenthesisedExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code booleanCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBooleanCall(_ ctx: CollyParser.BooleanCallContext)
	/**
	 * Exit a parse tree produced by the {@code booleanCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBooleanCall(_ ctx: CollyParser.BooleanCallContext)
	/**
	 * Enter a parse tree produced by the {@code booleanAndExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBooleanAndExpression(_ ctx: CollyParser.BooleanAndExpressionContext)
	/**
	 * Exit a parse tree produced by the {@code booleanAndExpression}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBooleanAndExpression(_ ctx: CollyParser.BooleanAndExpressionContext)
	/**
	 * Enter a parse tree produced by the {@code methodCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterMethodCall(_ ctx: CollyParser.MethodCallContext)
	/**
	 * Exit a parse tree produced by the {@code methodCall}
	 * labeled alternative in {@link CollyParser#expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitMethodCall(_ ctx: CollyParser.MethodCallContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#string}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterString(_ ctx: CollyParser.StringContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#string}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitString(_ ctx: CollyParser.StringContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#closure}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterClosure(_ ctx: CollyParser.ClosureContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#closure}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitClosure(_ ctx: CollyParser.ClosureContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#function_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterFunction_call(_ ctx: CollyParser.Function_callContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#function_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitFunction_call(_ ctx: CollyParser.Function_callContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#array}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterArray(_ ctx: CollyParser.ArrayContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#array}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitArray(_ ctx: CollyParser.ArrayContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#dictionary}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterDictionary(_ ctx: CollyParser.DictionaryContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#dictionary}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitDictionary(_ ctx: CollyParser.DictionaryContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#key_value_pair}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterKey_value_pair(_ ctx: CollyParser.Key_value_pairContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#key_value_pair}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitKey_value_pair(_ ctx: CollyParser.Key_value_pairContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterProperty_assign(_ ctx: CollyParser.Property_assignContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitProperty_assign(_ ctx: CollyParser.Property_assignContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#pattern_expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPattern_expr(_ ctx: CollyParser.Pattern_exprContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#pattern_expr}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPattern_expr(_ ctx: CollyParser.Pattern_exprContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#pattern}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPattern(_ ctx: CollyParser.PatternContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#pattern}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPattern(_ ctx: CollyParser.PatternContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#bar}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBar(_ ctx: CollyParser.BarContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#bar}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBar(_ ctx: CollyParser.BarContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#bar_property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterBar_property_assign(_ ctx: CollyParser.Bar_property_assignContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#bar_property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitBar_property_assign(_ ctx: CollyParser.Bar_property_assignContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#multibar_func_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterMultibar_func_call(_ ctx: CollyParser.Multibar_func_callContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#multibar_func_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitMultibar_func_call(_ ctx: CollyParser.Multibar_func_callContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#events_sequence}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterEvents_sequence(_ ctx: CollyParser.Events_sequenceContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#events_sequence}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitEvents_sequence(_ ctx: CollyParser.Events_sequenceContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#events_group}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterEvents_group(_ ctx: CollyParser.Events_groupContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#events_group}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitEvents_group(_ ctx: CollyParser.Events_groupContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#chord}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterChord(_ ctx: CollyParser.ChordContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#chord}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitChord(_ ctx: CollyParser.ChordContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#nested_events_group}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterNested_events_group(_ ctx: CollyParser.Nested_events_groupContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#nested_events_group}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitNested_events_group(_ ctx: CollyParser.Nested_events_groupContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#event_property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterEvent_property_assign(_ ctx: CollyParser.Event_property_assignContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#event_property_assign}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitEvent_property_assign(_ ctx: CollyParser.Event_property_assignContext)
	/**
	 * Enter a parse tree produced by {@link CollyParser#pat_func_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func enterPat_func_call(_ ctx: CollyParser.Pat_func_callContext)
	/**
	 * Exit a parse tree produced by {@link CollyParser#pat_func_call}.
	 - Parameters:
	   - ctx: the parse tree
	 */
	func exitPat_func_call(_ ctx: CollyParser.Pat_func_callContext)
}