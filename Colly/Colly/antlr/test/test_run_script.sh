# exporting classes
export CLASSPATH=".:/usr/local/lib/antlr-4.5.3-complete.jar:$CLASSPATH"

# build Java target
java -jar /usr/local/lib/antlr-4.5.3-complete.jar $(PWD)/../Colly.g4 -visitor -o $(PWD);

# compile generated files
javac $(PWD)/*.java;

# run test (currently not running, i don't know why)
# java org.antlr.v4.gui.TestRig $(PWD)/Colly file -gui $(PWD)/../TestInput.colly;
