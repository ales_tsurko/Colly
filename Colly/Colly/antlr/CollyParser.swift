// Generated from CollyParser.g4 by ANTLR 4.5.3
import Antlr4

open class CollyParser: Parser {

	internal static var _decisionToDFA: [DFA] = {
          var decisionToDFA = [DFA]()
          let length = CollyParser._ATN.getNumberOfDecisions()
          for i in 0..<length {
            decisionToDFA.append(DFA(CollyParser._ATN.getDecisionState(i)!, i))
           }
           return decisionToDFA
     }()
	internal static let _sharedContextCache: PredictionContextCache = PredictionContextCache()
	public static let IMPORT=1, LSP=2, RSP=3, LP=4, RP=5, L_CURLY=6, R_CURLY=7, 
                   VL=8, DOLLAR=9, DCOLON=10, COLON=11, SEMICOLON=12, DOT=13, 
                   QUOTE=14, RANGE=15, NUMBER=16, NOT=17, TILDE=18, MUL=19, 
                   DIV=20, MOD=21, PLUS=22, MINUS=23, LSHIFT=24, RSHIFT=25, 
                   LESS=26, LESSEQ=27, GREATER=28, GREATEREQ=29, EQUAL=30, 
                   NOTEQUAL=31, AND=32, XOR=33, OR=34, BOOL_AND=35, BOOL_OR=36, 
                   ASSIGN=37, MULASSIGN=38, DIVASSIGN=39, MODASSIGN=40, 
                   PLUSASSIGN=41, MINUSASSIGN=42, LSHIFTASSIGN=43, RSHIFTASSIGN=44, 
                   ANDASSIGN=45, XORASSIGN=46, ORASSIGN=47, BOOL=48, TRUE=49, 
                   FALSE=50, IF=51, ELSE=52, SWITCH=53, CASE=54, DEFAULT=55, 
                   WHILE=56, DO=57, UNTIL=58, RETURN=59, BREAK=60, CONTINUE=61, 
                   ID=62, LETTER=63, NEWLINE=64, WS=65, LINE_ESCAPE=66, 
                   COMMENT=67, OUT_QUOTE=68, ANYTHING=69, EVENT=70, NOTE=71, 
                   PAT_END=72, BARLINE=73, PAT_L_ANGL_BRCKT=74, PAT_R_ANGL_BRCKT=75, 
                   PAT_L_CURLY=76, PAT_COLON=77, PAT_WS=78, PAT_NEWLINE=79, 
                   PAT_COMMENT=80, PAT_LINE_ESCAPE=81
	public static let RULE_score = 0, RULE_end_stat = 1, RULE_stat = 2, RULE_return_stat = 3, 
                   RULE_func_def = 4, RULE_func_params = 5, RULE_if_stat = 6, 
                   RULE_else_if = 7, RULE_if_else = 8, RULE_switch_stat = 9, 
                   RULE_switch_case = 10, RULE_switch_default = 11, RULE_while_stat = 12, 
                   RULE_do_until_stat = 13, RULE_assignment = 14, RULE_expr = 15, 
                   RULE_string = 16, RULE_closure = 17, RULE_function_call = 18, 
                   RULE_array = 19, RULE_dictionary = 20, RULE_key_value_pair = 21, 
                   RULE_property_assign = 22, RULE_pattern_expr = 23, RULE_pattern = 24, 
                   RULE_bar = 25, RULE_bar_property_assign = 26, RULE_multibar_func_call = 27, 
                   RULE_events_sequence = 28, RULE_events_group = 29, RULE_chord = 30, 
                   RULE_nested_events_group = 31, RULE_event_property_assign = 32, 
                   RULE_pat_func_call = 33
	public static let ruleNames: [String] = [
		"score", "end_stat", "stat", "return_stat", "func_def", "func_params", 
		"if_stat", "else_if", "if_else", "switch_stat", "switch_case", "switch_default", 
		"while_stat", "do_until_stat", "assignment", "expr", "string", "closure", 
		"function_call", "array", "dictionary", "key_value_pair", "property_assign", 
		"pattern_expr", "pattern", "bar", "bar_property_assign", "multibar_func_call", 
		"events_sequence", "events_group", "chord", "nested_events_group", "event_property_assign", 
		"pat_func_call"
	]

	private static let _LITERAL_NAMES: [String?] = [
		nil, "'import'", "'['", nil, nil, nil, nil, "'}'", nil, "'$'", "'::'", 
		nil, nil, "'.'", nil, nil, nil, "'!'", "'~'", "'*'", "'/'", "'%'", "'+'", 
		"'-'", "'<<'", "'>>'", nil, "'<='", nil, "'>='", "'=='", "'!='", "'&'", 
		"'^'", "'bor'", "'&&'", "'||'", "'='", "'*='", "'/='", "'%='", "'+='", 
		"'-='", "'<<='", "'>>='", "'&='", "'^='", "'|='", nil, "'true'", "'false'", 
		"'if'", "'else'", "'switch'", "'case'", "'default'", "'while'", "'do'", 
		"'until'", "'return'", "'break'", "'continue'"
	]
	private static let _SYMBOLIC_NAMES: [String?] = [
		nil, "IMPORT", "LSP", "RSP", "LP", "RP", "L_CURLY", "R_CURLY", "VL", "DOLLAR", 
		"DCOLON", "COLON", "SEMICOLON", "DOT", "QUOTE", "RANGE", "NUMBER", "NOT", 
		"TILDE", "MUL", "DIV", "MOD", "PLUS", "MINUS", "LSHIFT", "RSHIFT", "LESS", 
		"LESSEQ", "GREATER", "GREATEREQ", "EQUAL", "NOTEQUAL", "AND", "XOR", "OR", 
		"BOOL_AND", "BOOL_OR", "ASSIGN", "MULASSIGN", "DIVASSIGN", "MODASSIGN", 
		"PLUSASSIGN", "MINUSASSIGN", "LSHIFTASSIGN", "RSHIFTASSIGN", "ANDASSIGN", 
		"XORASSIGN", "ORASSIGN", "BOOL", "TRUE", "FALSE", "IF", "ELSE", "SWITCH", 
		"CASE", "DEFAULT", "WHILE", "DO", "UNTIL", "RETURN", "BREAK", "CONTINUE", 
		"ID", "LETTER", "NEWLINE", "WS", "LINE_ESCAPE", "COMMENT", "OUT_QUOTE", 
		"ANYTHING", "EVENT", "NOTE", "PAT_END", "BARLINE", "PAT_L_ANGL_BRCKT", 
		"PAT_R_ANGL_BRCKT", "PAT_L_CURLY", "PAT_COLON", "PAT_WS", "PAT_NEWLINE", 
		"PAT_COMMENT", "PAT_LINE_ESCAPE"
	]
	public static let VOCABULARY: Vocabulary = Vocabulary(_LITERAL_NAMES, _SYMBOLIC_NAMES)

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	//@Deprecated
	public let tokenNames: [String?]? = {
	    let length = _SYMBOLIC_NAMES.count
	    var tokenNames = [String?](repeating: nil, count: length)
		for i in 0..<length {
			var name = VOCABULARY.getLiteralName(i)
			if name == nil {
				name = VOCABULARY.getSymbolicName(i)
			}
			if name == nil {
				name = "<INVALID>"
			}
			tokenNames[i] = name
		}
		return tokenNames
	}()

	override
	open func getTokenNames() -> [String?]? {
		return tokenNames
	}

	override
	open func getGrammarFileName() -> String { return "CollyParser.g4" }

	override
	open func getRuleNames() -> [String] { return CollyParser.ruleNames }

	override
	open func getSerializedATN() -> String { return CollyParser._serializedATN }

	override
	open func getATN() -> ATN { return CollyParser._ATN }

	open override func getVocabulary() -> Vocabulary {
	    return CollyParser.VOCABULARY
	}

	public override init(_ input:TokenStream)throws {
	    RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION)
		try super.init(input)
		_interp = ParserATNSimulator(self,CollyParser._ATN,CollyParser._decisionToDFA, CollyParser._sharedContextCache)
	}
	open class ScoreContext:ParserRuleContext {
		open func end_stat() -> Array<End_statContext> {
			return getRuleContexts(End_statContext.self)
		}
		open func end_stat(_ i: Int) -> End_statContext? {
			return getRuleContext(End_statContext.self,i)
		}
		open func EOF() -> TerminalNode? { return getToken(CollyParser.EOF, 0) }
		open override func getRuleIndex() -> Int { return CollyParser.RULE_score }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterScore(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitScore(self)
			}
		}
	}
	@discardableResult
	open func score() throws -> ScoreContext {
		var _localctx: ScoreContext = ScoreContext(_ctx, getState())
		try enterRule(_localctx, 0, CollyParser.RULE_score)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(69) 
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	repeat {
		 		setState(68)
		 		try end_stat()


		 		setState(71); 
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	} while (//closure
		 	 { () -> Bool in
		 	      var testSet: Bool = {  () -> Bool in
		 	   let testArray: [Int] = [_la, CollyParser.IMPORT,CollyParser.LSP,CollyParser.LP,CollyParser.VL,CollyParser.DOLLAR,CollyParser.COLON,CollyParser.QUOTE,CollyParser.RANGE,CollyParser.NUMBER,CollyParser.NOT,CollyParser.TILDE,CollyParser.PLUS,CollyParser.MINUS,CollyParser.BOOL,CollyParser.IF,CollyParser.SWITCH,CollyParser.WHILE,CollyParser.DO,CollyParser.RETURN,CollyParser.BREAK,CollyParser.CONTINUE,CollyParser.ID]
		 	    return  Utils.testBitLeftShiftArray(testArray, 0)
		 	}()
		 	          testSet = testSet || {  () -> Bool in
		 	             let testArray: [Int] = [_la, CollyParser.NEWLINE,CollyParser.EVENT,CollyParser.BARLINE,CollyParser.PAT_L_ANGL_BRCKT,CollyParser.PAT_L_CURLY,CollyParser.PAT_WS]
		 	              return  Utils.testBitLeftShiftArray(testArray, 64)
		 	          }()
		 	      return testSet
		 	 }())
		 	setState(74)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,1,_ctx)) {
		 	case 1:
		 		setState(73)
		 		try match(CollyParser.EOF)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class End_statContext:ParserRuleContext {
		open func IMPORT() -> TerminalNode? { return getToken(CollyParser.IMPORT, 0) }
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func NEWLINE() -> TerminalNode? { return getToken(CollyParser.NEWLINE, 0) }
		open func EOF() -> TerminalNode? { return getToken(CollyParser.EOF, 0) }
		open func stat() -> StatContext? {
			return getRuleContext(StatContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_end_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterEnd_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitEnd_stat(self)
			}
		}
	}
	@discardableResult
	open func end_stat() throws -> End_statContext {
		var _localctx: End_statContext = End_statContext(_ctx, getState())
		try enterRule(_localctx, 2, CollyParser.RULE_end_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	setState(83)
		 	try _errHandler.sync(self)
		 	switch (try _input.LA(1)) {
		 	case CollyParser.IMPORT:
		 		try enterOuterAlt(_localctx, 1)
		 		setState(76)
		 		try match(CollyParser.IMPORT)
		 		setState(77)
		 		try match(CollyParser.ID)
		 		setState(78)
		 		_la = try _input.LA(1)
		 		if (!(//closure
		 		 { () -> Bool in
		 		      var testSet: Bool = _la == CollyParser.EOF
		 		          testSet = testSet || _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }())) {
		 		try _errHandler.recoverInline(self)
		 		} else {
		 			try consume()
		 		}

		 		break
		 	case CollyParser.LSP:fallthrough
		 	case CollyParser.LP:fallthrough
		 	case CollyParser.VL:fallthrough
		 	case CollyParser.DOLLAR:fallthrough
		 	case CollyParser.COLON:fallthrough
		 	case CollyParser.QUOTE:fallthrough
		 	case CollyParser.RANGE:fallthrough
		 	case CollyParser.NUMBER:fallthrough
		 	case CollyParser.NOT:fallthrough
		 	case CollyParser.TILDE:fallthrough
		 	case CollyParser.PLUS:fallthrough
		 	case CollyParser.MINUS:fallthrough
		 	case CollyParser.BOOL:fallthrough
		 	case CollyParser.IF:fallthrough
		 	case CollyParser.SWITCH:fallthrough
		 	case CollyParser.WHILE:fallthrough
		 	case CollyParser.DO:fallthrough
		 	case CollyParser.RETURN:fallthrough
		 	case CollyParser.BREAK:fallthrough
		 	case CollyParser.CONTINUE:fallthrough
		 	case CollyParser.ID:fallthrough
		 	case CollyParser.EVENT:fallthrough
		 	case CollyParser.BARLINE:fallthrough
		 	case CollyParser.PAT_L_ANGL_BRCKT:fallthrough
		 	case CollyParser.PAT_L_CURLY:fallthrough
		 	case CollyParser.PAT_WS:
		 		try enterOuterAlt(_localctx, 2)
		 		setState(79)
		 		try stat()
		 		setState(80)
		 		_la = try _input.LA(1)
		 		if (!(//closure
		 		 { () -> Bool in
		 		      var testSet: Bool = _la == CollyParser.EOF
		 		          testSet = testSet || _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }())) {
		 		try _errHandler.recoverInline(self)
		 		} else {
		 			try consume()
		 		}

		 		break

		 	case CollyParser.NEWLINE:
		 		try enterOuterAlt(_localctx, 3)
		 		setState(82)
		 		try match(CollyParser.NEWLINE)

		 		break
		 	default:
		 		throw try ANTLRException.recognition(e: NoViableAltException(self))
		 	}
		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class StatContext:ParserRuleContext {
		open override func getRuleIndex() -> Int { return CollyParser.RULE_stat }
	 
		public  func copyFrom(_ ctx: StatContext) {
			super.copyFrom(ctx)
		}
	}
	public  final class WhileStatementContext: StatContext {
		open func while_stat() -> While_statContext? {
			return getRuleContext(While_statContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterWhileStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitWhileStatement(self)
			}
		}
	}
	public  final class DoUntilStatementContext: StatContext {
		open func do_until_stat() -> Do_until_statContext? {
			return getRuleContext(Do_until_statContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterDoUntilStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitDoUntilStatement(self)
			}
		}
	}
	public  final class FunctionDefinitionContext: StatContext {
		open func func_def() -> Func_defContext? {
			return getRuleContext(Func_defContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterFunctionDefinition(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitFunctionDefinition(self)
			}
		}
	}
	public  final class AssignStatementContext: StatContext {
		open func assignment() -> AssignmentContext? {
			return getRuleContext(AssignmentContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterAssignStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitAssignStatement(self)
			}
		}
	}
	public  final class BreakStatementContext: StatContext {
		open func BREAK() -> TerminalNode? { return getToken(CollyParser.BREAK, 0) }
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBreakStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBreakStatement(self)
			}
		}
	}
	public  final class ExpressionStatementContext: StatContext {
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterExpressionStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitExpressionStatement(self)
			}
		}
	}
	public  final class ContinueStatementContext: StatContext {
		open func CONTINUE() -> TerminalNode? { return getToken(CollyParser.CONTINUE, 0) }
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterContinueStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitContinueStatement(self)
			}
		}
	}
	public  final class IfStatementContext: StatContext {
		open func if_stat() -> If_statContext? {
			return getRuleContext(If_statContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterIfStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitIfStatement(self)
			}
		}
	}
	public  final class ReturnStatementContext: StatContext {
		open func return_stat() -> Return_statContext? {
			return getRuleContext(Return_statContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterReturnStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitReturnStatement(self)
			}
		}
	}
	public  final class SwitchStatementContext: StatContext {
		open func switch_stat() -> Switch_statContext? {
			return getRuleContext(Switch_statContext.self,0)
		}
		public init(_ ctx: StatContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSwitchStatement(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSwitchStatement(self)
			}
		}
	}
	@discardableResult
	open func stat() throws -> StatContext {
		var _localctx: StatContext = StatContext(_ctx, getState())
		try enterRule(_localctx, 4, CollyParser.RULE_stat)
		defer {
	    		try! exitRule()
	    }
		do {
		 	setState(95)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,3, _ctx)) {
		 	case 1:
		 		_localctx =  ExpressionStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 1)
		 		setState(85)
		 		try expr(0)

		 		break
		 	case 2:
		 		_localctx =  AssignStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 2)
		 		setState(86)
		 		try assignment()

		 		break
		 	case 3:
		 		_localctx =  FunctionDefinitionContext(_localctx);
		 		try enterOuterAlt(_localctx, 3)
		 		setState(87)
		 		try func_def()

		 		break
		 	case 4:
		 		_localctx =  IfStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 4)
		 		setState(88)
		 		try if_stat()

		 		break
		 	case 5:
		 		_localctx =  SwitchStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 5)
		 		setState(89)
		 		try switch_stat()

		 		break
		 	case 6:
		 		_localctx =  WhileStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 6)
		 		setState(90)
		 		try while_stat()

		 		break
		 	case 7:
		 		_localctx =  DoUntilStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 7)
		 		setState(91)
		 		try do_until_stat()

		 		break
		 	case 8:
		 		_localctx =  ReturnStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 8)
		 		setState(92)
		 		try return_stat()

		 		break
		 	case 9:
		 		_localctx =  BreakStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 9)
		 		setState(93)
		 		try match(CollyParser.BREAK)

		 		break
		 	case 10:
		 		_localctx =  ContinueStatementContext(_localctx);
		 		try enterOuterAlt(_localctx, 10)
		 		setState(94)
		 		try match(CollyParser.CONTINUE)

		 		break
		 	default: break
		 	}
		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Return_statContext:ParserRuleContext {
		open func RETURN() -> TerminalNode? { return getToken(CollyParser.RETURN, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> StatContext? {
			return getRuleContext(StatContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_return_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterReturn_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitReturn_stat(self)
			}
		}
	}
	@discardableResult
	open func return_stat() throws -> Return_statContext {
		var _localctx: Return_statContext = Return_statContext(_ctx, getState())
		try enterRule(_localctx, 6, CollyParser.RULE_return_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(97)
		 	try match(CollyParser.RETURN)
		 	setState(101)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,4,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(98)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(103)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,4,_ctx)
		 	}
		 	setState(111)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,7,_ctx)) {
		 	case 1:
		 		setState(105)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		if (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(104)
		 			try match(CollyParser.NEWLINE)

		 		}

		 		setState(107)
		 		try stat()
		 		setState(109)
		 		try _errHandler.sync(self)
		 		switch (try getInterpreter().adaptivePredict(_input,6,_ctx)) {
		 		case 1:
		 			setState(108)
		 			try match(CollyParser.NEWLINE)

		 			break
		 		default: break
		 		}

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Func_defContext:ParserRuleContext {
		open func COLON() -> TerminalNode? { return getToken(CollyParser.COLON, 0) }
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func LP() -> TerminalNode? { return getToken(CollyParser.LP, 0) }
		open func RP() -> TerminalNode? { return getToken(CollyParser.RP, 0) }
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func func_params() -> Array<Func_paramsContext> {
			return getRuleContexts(Func_paramsContext.self)
		}
		open func func_params(_ i: Int) -> Func_paramsContext? {
			return getRuleContext(Func_paramsContext.self,i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_func_def }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterFunc_def(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitFunc_def(self)
			}
		}
	}
	@discardableResult
	open func func_def() throws -> Func_defContext {
		var _localctx: Func_defContext = Func_defContext(_ctx, getState())
		try enterRule(_localctx, 8, CollyParser.RULE_func_def)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(113)
		 	try match(CollyParser.COLON)
		 	setState(117)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(114)
		 		try match(CollyParser.NEWLINE)


		 		setState(119)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(120)
		 	try match(CollyParser.ID)
		 	setState(124)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(121)
		 		try match(CollyParser.NEWLINE)


		 		setState(126)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(127)
		 	try match(CollyParser.LP)
		 	setState(131)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,10,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(128)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(133)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,10,_ctx)
		 	}
		 	setState(137)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,11,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(134)
		 			try func_params()

		 	 
		 		}
		 		setState(139)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,11,_ctx)
		 	}
		 	setState(143)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(140)
		 		try match(CollyParser.NEWLINE)


		 		setState(145)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(146)
		 	try match(CollyParser.RP)
		 	setState(150)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,13,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(147)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(152)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,13,_ctx)
		 	}
		 	setState(160); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(154)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(153)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(156)
		 			try stat()
		 			setState(158)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,15,_ctx)) {
		 			case 1:
		 				setState(157)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(162); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,16,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(167)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(164)
		 		try match(CollyParser.NEWLINE)


		 		setState(169)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(170)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Func_paramsContext:ParserRuleContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func assignment() -> AssignmentContext? {
			return getRuleContext(AssignmentContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_func_params }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterFunc_params(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitFunc_params(self)
			}
		}
	}
	@discardableResult
	open func func_params() throws -> Func_paramsContext {
		var _localctx: Func_paramsContext = Func_paramsContext(_ctx, getState())
		try enterRule(_localctx, 10, CollyParser.RULE_func_params)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(173)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(172)
		 		try match(CollyParser.NEWLINE)

		 	}

		 	setState(177)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,19, _ctx)) {
		 	case 1:
		 		setState(175)
		 		try match(CollyParser.ID)

		 		break
		 	case 2:
		 		setState(176)
		 		try assignment()

		 		break
		 	default: break
		 	}
		 	setState(180)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,20,_ctx)) {
		 	case 1:
		 		setState(179)
		 		try match(CollyParser.NEWLINE)

		 		break
		 	default: break
		 	}


		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class If_statContext:ParserRuleContext {
		open func IF() -> TerminalNode? { return getToken(CollyParser.IF, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open func else_if() -> Array<Else_ifContext> {
			return getRuleContexts(Else_ifContext.self)
		}
		open func else_if(_ i: Int) -> Else_ifContext? {
			return getRuleContext(Else_ifContext.self,i)
		}
		open func if_else() -> If_elseContext? {
			return getRuleContext(If_elseContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_if_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterIf_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitIf_stat(self)
			}
		}
	}
	@discardableResult
	open func if_stat() throws -> If_statContext {
		var _localctx: If_statContext = If_statContext(_ctx, getState())
		try enterRule(_localctx, 12, CollyParser.RULE_if_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(182)
		 	try match(CollyParser.IF)
		 	setState(186)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(183)
		 		try match(CollyParser.NEWLINE)


		 		setState(188)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(189)
		 	try expr(0)
		 	setState(193)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,22,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(190)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(195)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,22,_ctx)
		 	}
		 	setState(203); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(197)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(196)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(199)
		 			try stat()
		 			setState(201)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,24,_ctx)) {
		 			case 1:
		 				setState(200)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(205); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,25,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(210)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(207)
		 		try match(CollyParser.NEWLINE)


		 		setState(212)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(216)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,27,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(213)
		 			try else_if()

		 	 
		 		}
		 		setState(218)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,27,_ctx)
		 	}
		 	setState(220)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.ELSE
		 	      return testSet
		 	 }()) {
		 		setState(219)
		 		try if_else()

		 	}

		 	setState(222)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Else_ifContext:ParserRuleContext {
		open func ELSE() -> TerminalNode? { return getToken(CollyParser.ELSE, 0) }
		open func IF() -> TerminalNode? { return getToken(CollyParser.IF, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_else_if }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterElse_if(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitElse_if(self)
			}
		}
	}
	@discardableResult
	open func else_if() throws -> Else_ifContext {
		var _localctx: Else_ifContext = Else_ifContext(_ctx, getState())
		try enterRule(_localctx, 14, CollyParser.RULE_else_if)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(224)
		 	try match(CollyParser.ELSE)
		 	setState(225)
		 	try match(CollyParser.IF)
		 	setState(229)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(226)
		 		try match(CollyParser.NEWLINE)


		 		setState(231)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(232)
		 	try expr(0)
		 	setState(236)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,30,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(233)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(238)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,30,_ctx)
		 	}
		 	setState(246); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(240)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(239)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(242)
		 			try stat()
		 			setState(244)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,32,_ctx)) {
		 			case 1:
		 				setState(243)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(248); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,33,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(253)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(250)
		 		try match(CollyParser.NEWLINE)


		 		setState(255)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class If_elseContext:ParserRuleContext {
		open func ELSE() -> TerminalNode? { return getToken(CollyParser.ELSE, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_if_else }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterIf_else(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitIf_else(self)
			}
		}
	}
	@discardableResult
	open func if_else() throws -> If_elseContext {
		var _localctx: If_elseContext = If_elseContext(_ctx, getState())
		try enterRule(_localctx, 16, CollyParser.RULE_if_else)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(256)
		 	try match(CollyParser.ELSE)
		 	setState(260)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,35,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(257)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(262)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,35,_ctx)
		 	}
		 	setState(270); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(264)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(263)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(266)
		 			try stat()
		 			setState(268)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,37,_ctx)) {
		 			case 1:
		 				setState(267)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(272); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,38,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(277)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(274)
		 		try match(CollyParser.NEWLINE)


		 		setState(279)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Switch_statContext:ParserRuleContext {
		open func SWITCH() -> TerminalNode? { return getToken(CollyParser.SWITCH, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func switch_default() -> Switch_defaultContext? {
			return getRuleContext(Switch_defaultContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func switch_case() -> Array<Switch_caseContext> {
			return getRuleContexts(Switch_caseContext.self)
		}
		open func switch_case(_ i: Int) -> Switch_caseContext? {
			return getRuleContext(Switch_caseContext.self,i)
		}
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open override func getRuleIndex() -> Int { return CollyParser.RULE_switch_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSwitch_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSwitch_stat(self)
			}
		}
	}
	@discardableResult
	open func switch_stat() throws -> Switch_statContext {
		var _localctx: Switch_statContext = Switch_statContext(_ctx, getState())
		try enterRule(_localctx, 18, CollyParser.RULE_switch_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(280)
		 	try match(CollyParser.SWITCH)
		 	setState(284)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(281)
		 		try match(CollyParser.NEWLINE)


		 		setState(286)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(287)
		 	try expr(0)
		 	setState(291)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(288)
		 		try match(CollyParser.NEWLINE)


		 		setState(293)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(301) 
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	repeat {
		 		setState(294)
		 		try switch_case()
		 		setState(298)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(295)
		 			try match(CollyParser.NEWLINE)


		 			setState(300)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}


		 		setState(303); 
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	} while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.CASE
		 	      return testSet
		 	 }())
		 	setState(305)
		 	try switch_default()
		 	setState(309)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,44,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(306)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(311)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,44,_ctx)
		 	}
		 	setState(313)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,45,_ctx)) {
		 	case 1:
		 		setState(312)
		 		try match(CollyParser.SEMICOLON)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Switch_caseContext:ParserRuleContext {
		open func CASE() -> TerminalNode? { return getToken(CollyParser.CASE, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_switch_case }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSwitch_case(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSwitch_case(self)
			}
		}
	}
	@discardableResult
	open func switch_case() throws -> Switch_caseContext {
		var _localctx: Switch_caseContext = Switch_caseContext(_ctx, getState())
		try enterRule(_localctx, 20, CollyParser.RULE_switch_case)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(315)
		 	try match(CollyParser.CASE)
		 	setState(319)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(316)
		 		try match(CollyParser.NEWLINE)


		 		setState(321)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(322)
		 	try expr(0)
		 	setState(326)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,47,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(323)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(328)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,47,_ctx)
		 	}
		 	setState(336); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(330)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(329)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(332)
		 			try stat()
		 			setState(334)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,49,_ctx)) {
		 			case 1:
		 				setState(333)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(338); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,50,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(343)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(340)
		 		try match(CollyParser.NEWLINE)


		 		setState(345)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(346)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Switch_defaultContext:ParserRuleContext {
		open func DEFAULT() -> TerminalNode? { return getToken(CollyParser.DEFAULT, 0) }
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_switch_default }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSwitch_default(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSwitch_default(self)
			}
		}
	}
	@discardableResult
	open func switch_default() throws -> Switch_defaultContext {
		var _localctx: Switch_defaultContext = Switch_defaultContext(_ctx, getState())
		try enterRule(_localctx, 22, CollyParser.RULE_switch_default)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(348)
		 	try match(CollyParser.DEFAULT)
		 	setState(352)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,52,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(349)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(354)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,52,_ctx)
		 	}
		 	setState(362); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(356)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(355)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(358)
		 			try stat()
		 			setState(360)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,54,_ctx)) {
		 			case 1:
		 				setState(359)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(364); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,55,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(369)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(366)
		 		try match(CollyParser.NEWLINE)


		 		setState(371)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(372)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class While_statContext:ParserRuleContext {
		open func WHILE() -> TerminalNode? { return getToken(CollyParser.WHILE, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_while_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterWhile_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitWhile_stat(self)
			}
		}
	}
	@discardableResult
	open func while_stat() throws -> While_statContext {
		var _localctx: While_statContext = While_statContext(_ctx, getState())
		try enterRule(_localctx, 24, CollyParser.RULE_while_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(374)
		 	try match(CollyParser.WHILE)
		 	setState(378)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(375)
		 		try match(CollyParser.NEWLINE)


		 		setState(380)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(381)
		 	try expr(0)
		 	setState(385)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,58,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(382)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(387)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,58,_ctx)
		 	}
		 	setState(395); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(389)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(388)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(391)
		 			try stat()
		 			setState(393)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,60,_ctx)) {
		 			case 1:
		 				setState(392)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(397); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,61,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(402)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(399)
		 		try match(CollyParser.NEWLINE)


		 		setState(404)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(405)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Do_until_statContext:ParserRuleContext {
		open func DO() -> TerminalNode? { return getToken(CollyParser.DO, 0) }
		open func UNTIL() -> TerminalNode? { return getToken(CollyParser.UNTIL, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open override func getRuleIndex() -> Int { return CollyParser.RULE_do_until_stat }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterDo_until_stat(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitDo_until_stat(self)
			}
		}
	}
	@discardableResult
	open func do_until_stat() throws -> Do_until_statContext {
		var _localctx: Do_until_statContext = Do_until_statContext(_ctx, getState())
		try enterRule(_localctx, 26, CollyParser.RULE_do_until_stat)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(407)
		 	try match(CollyParser.DO)
		 	setState(411)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,63,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(408)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(413)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,63,_ctx)
		 	}
		 	setState(421); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(415)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(414)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(417)
		 			try stat()
		 			setState(419)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,65,_ctx)) {
		 			case 1:
		 				setState(418)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(423); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,66,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(428)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(425)
		 		try match(CollyParser.NEWLINE)


		 		setState(430)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(431)
		 	try match(CollyParser.UNTIL)
		 	setState(435)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(432)
		 		try match(CollyParser.NEWLINE)


		 		setState(437)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(438)
		 	try expr(0)
		 	setState(440)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,69,_ctx)) {
		 	case 1:
		 		setState(439)
		 		try match(CollyParser.SEMICOLON)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class AssignmentContext:ParserRuleContext {
		open override func getRuleIndex() -> Int { return CollyParser.RULE_assignment }
	 
		public  func copyFrom(_ ctx: AssignmentContext) {
			super.copyFrom(ctx)
		}
	}
	public  final class ExpressionAssignmentContext: AssignmentContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func ASSIGN() -> TerminalNode? { return getToken(CollyParser.ASSIGN, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: AssignmentContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterExpressionAssignment(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitExpressionAssignment(self)
			}
		}
	}
	public  final class PatternAssignmentContext: AssignmentContext {
		open func DOLLAR() -> TerminalNode? { return getToken(CollyParser.DOLLAR, 0) }
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: AssignmentContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPatternAssignment(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPatternAssignment(self)
			}
		}
	}
	@discardableResult
	open func assignment() throws -> AssignmentContext {
		var _localctx: AssignmentContext = AssignmentContext(_ctx, getState())
		try enterRule(_localctx, 28, CollyParser.RULE_assignment)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	setState(466)
		 	try _errHandler.sync(self)
		 	switch (try _input.LA(1)) {
		 	case CollyParser.ID:
		 		_localctx =  ExpressionAssignmentContext(_localctx);
		 		try enterOuterAlt(_localctx, 1)
		 		setState(442)
		 		try match(CollyParser.ID)
		 		setState(444)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		if (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(443)
		 			try match(CollyParser.NEWLINE)

		 		}

		 		setState(446)
		 		try match(CollyParser.ASSIGN)
		 		setState(448)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		if (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(447)
		 			try match(CollyParser.NEWLINE)

		 		}

		 		setState(450)
		 		try expr(0)

		 		break

		 	case CollyParser.DOLLAR:
		 		_localctx =  PatternAssignmentContext(_localctx);
		 		try enterOuterAlt(_localctx, 2)
		 		setState(451)
		 		try match(CollyParser.DOLLAR)
		 		setState(455)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(452)
		 			try match(CollyParser.NEWLINE)


		 			setState(457)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}
		 		setState(458)
		 		try match(CollyParser.ID)
		 		setState(462)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(459)
		 			try match(CollyParser.NEWLINE)


		 			setState(464)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}
		 		setState(465)
		 		try expr(0)

		 		break
		 	default:
		 		throw try ANTLRException.recognition(e: NoViableAltException(self))
		 	}
		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}

	open class ExprContext:ParserRuleContext {
		open override func getRuleIndex() -> Int { return CollyParser.RULE_expr }
	 
		public  func copyFrom(_ ctx: ExprContext) {
			super.copyFrom(ctx)
		}
	}
	public  final class BinaryOrExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func OR() -> TerminalNode? { return getToken(CollyParser.OR, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBinaryOrExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBinaryOrExpression(self)
			}
		}
	}
	public  final class BinaryXorExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func XOR() -> TerminalNode? { return getToken(CollyParser.XOR, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBinaryXorExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBinaryXorExpression(self)
			}
		}
	}
	public  final class UnaryPlusMinusContext: ExprContext {
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func PLUS() -> TerminalNode? { return getToken(CollyParser.PLUS, 0) }
		open func MINUS() -> TerminalNode? { return getToken(CollyParser.MINUS, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterUnaryPlusMinus(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitUnaryPlusMinus(self)
			}
		}
	}
	public  final class RangeCallContext: ExprContext {
		open func RANGE() -> TerminalNode? { return getToken(CollyParser.RANGE, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterRangeCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitRangeCall(self)
			}
		}
	}
	public  final class NumberCallContext: ExprContext {
		open func NUMBER() -> TerminalNode? { return getToken(CollyParser.NUMBER, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterNumberCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitNumberCall(self)
			}
		}
	}
	public  final class IdCallContext: ExprContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterIdCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitIdCall(self)
			}
		}
	}
	public  final class BinaryAndExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func AND() -> TerminalNode? { return getToken(CollyParser.AND, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBinaryAndExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBinaryAndExpression(self)
			}
		}
	}
	public  final class ExprThenAssignContext: ExprContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func MULASSIGN() -> TerminalNode? { return getToken(CollyParser.MULASSIGN, 0) }
		open func DIVASSIGN() -> TerminalNode? { return getToken(CollyParser.DIVASSIGN, 0) }
		open func MODASSIGN() -> TerminalNode? { return getToken(CollyParser.MODASSIGN, 0) }
		open func PLUSASSIGN() -> TerminalNode? { return getToken(CollyParser.PLUSASSIGN, 0) }
		open func MINUSASSIGN() -> TerminalNode? { return getToken(CollyParser.MINUSASSIGN, 0) }
		open func LSHIFTASSIGN() -> TerminalNode? { return getToken(CollyParser.LSHIFTASSIGN, 0) }
		open func RSHIFTASSIGN() -> TerminalNode? { return getToken(CollyParser.RSHIFTASSIGN, 0) }
		open func ANDASSIGN() -> TerminalNode? { return getToken(CollyParser.ANDASSIGN, 0) }
		open func XORASSIGN() -> TerminalNode? { return getToken(CollyParser.XORASSIGN, 0) }
		open func ORASSIGN() -> TerminalNode? { return getToken(CollyParser.ORASSIGN, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterExprThenAssign(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitExprThenAssign(self)
			}
		}
	}
	public  final class DictionaryCallContext: ExprContext {
		open func dictionary() -> DictionaryContext? {
			return getRuleContext(DictionaryContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterDictionaryCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitDictionaryCall(self)
			}
		}
	}
	public  final class PropertyAssignContext: ExprContext {
		open func property_assign() -> Property_assignContext? {
			return getRuleContext(Property_assignContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPropertyAssign(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPropertyAssign(self)
			}
		}
	}
	public  final class DotCallContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func DOT() -> TerminalNode? { return getToken(CollyParser.DOT, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterDotCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitDotCall(self)
			}
		}
	}
	public  final class ArrayCallContext: ExprContext {
		open func array() -> ArrayContext? {
			return getRuleContext(ArrayContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterArrayCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitArrayCall(self)
			}
		}
	}
	public  final class LessGreaterExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func LESS() -> TerminalNode? { return getToken(CollyParser.LESS, 0) }
		open func LESSEQ() -> TerminalNode? { return getToken(CollyParser.LESSEQ, 0) }
		open func GREATER() -> TerminalNode? { return getToken(CollyParser.GREATER, 0) }
		open func GREATEREQ() -> TerminalNode? { return getToken(CollyParser.GREATEREQ, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterLessGreaterExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitLessGreaterExpression(self)
			}
		}
	}
	public  final class MulDivModExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func MUL() -> TerminalNode? { return getToken(CollyParser.MUL, 0) }
		open func DIV() -> TerminalNode? { return getToken(CollyParser.DIV, 0) }
		open func MOD() -> TerminalNode? { return getToken(CollyParser.MOD, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterMulDivModExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitMulDivModExpression(self)
			}
		}
	}
	public  final class PatternExpressionContext: ExprContext {
		open func pattern_expr() -> Pattern_exprContext? {
			return getRuleContext(Pattern_exprContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPatternExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPatternExpression(self)
			}
		}
	}
	public  final class BinaryShiftContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func LSHIFT() -> TerminalNode? { return getToken(CollyParser.LSHIFT, 0) }
		open func RSHIFT() -> TerminalNode? { return getToken(CollyParser.RSHIFT, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBinaryShift(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBinaryShift(self)
			}
		}
	}
	public  final class BooleanOrExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func BOOL_OR() -> TerminalNode? { return getToken(CollyParser.BOOL_OR, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBooleanOrExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBooleanOrExpression(self)
			}
		}
	}
	public  final class SubscriptCallContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func LSP() -> Array<TerminalNode> { return getTokens(CollyParser.LSP) }
		open func LSP(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.LSP, i)
		}
		open func RSP() -> Array<TerminalNode> { return getTokens(CollyParser.RSP) }
		open func RSP(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.RSP, i)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSubscriptCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSubscriptCall(self)
			}
		}
	}
	public  final class LogicalAndBinaryNegationContext: ExprContext {
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NOT() -> TerminalNode? { return getToken(CollyParser.NOT, 0) }
		open func TILDE() -> TerminalNode? { return getToken(CollyParser.TILDE, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterLogicalAndBinaryNegation(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitLogicalAndBinaryNegation(self)
			}
		}
	}
	public  final class EqualityExpsressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func EQUAL() -> TerminalNode? { return getToken(CollyParser.EQUAL, 0) }
		open func NOTEQUAL() -> TerminalNode? { return getToken(CollyParser.NOTEQUAL, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterEqualityExpsression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitEqualityExpsression(self)
			}
		}
	}
	public  final class FunctionCallContext: ExprContext {
		open func function_call() -> Function_callContext? {
			return getRuleContext(Function_callContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterFunctionCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitFunctionCall(self)
			}
		}
	}
	public  final class ClosureExpressionContext: ExprContext {
		open func COLON() -> TerminalNode? { return getToken(CollyParser.COLON, 0) }
		open func closure() -> ClosureContext? {
			return getRuleContext(ClosureContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterClosureExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitClosureExpression(self)
			}
		}
	}
	public  final class SumExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func PLUS() -> TerminalNode? { return getToken(CollyParser.PLUS, 0) }
		open func MINUS() -> TerminalNode? { return getToken(CollyParser.MINUS, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterSumExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitSumExpression(self)
			}
		}
	}
	public  final class StringCallContext: ExprContext {
		open func string() -> StringContext? {
			return getRuleContext(StringContext.self,0)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterStringCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitStringCall(self)
			}
		}
	}
	public  final class ParenthesisedExpressionContext: ExprContext {
		open func LP() -> TerminalNode? { return getToken(CollyParser.LP, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func RP() -> TerminalNode? { return getToken(CollyParser.RP, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterParenthesisedExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitParenthesisedExpression(self)
			}
		}
	}
	public  final class BooleanCallContext: ExprContext {
		open func BOOL() -> TerminalNode? { return getToken(CollyParser.BOOL, 0) }
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBooleanCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBooleanCall(self)
			}
		}
	}
	public  final class BooleanAndExpressionContext: ExprContext {
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func BOOL_AND() -> TerminalNode? { return getToken(CollyParser.BOOL_AND, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBooleanAndExpression(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBooleanAndExpression(self)
			}
		}
	}
	public  final class MethodCallContext: ExprContext {
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func DCOLON() -> TerminalNode? { return getToken(CollyParser.DCOLON, 0) }
		open func closure() -> ClosureContext? {
			return getRuleContext(ClosureContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		public init(_ ctx: ExprContext) {
			super.init()
			copyFrom(ctx)
		}
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterMethodCall(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitMethodCall(self)
			}
		}
	}

	public final  func expr( ) throws -> ExprContext   {
		return try expr(0)
	}
	@discardableResult
	private func expr(_ _p: Int) throws -> ExprContext   {
		let _parentctx: ParserRuleContext? = _ctx
		var _parentState: Int = getState()
		var _localctx: ExprContext = ExprContext(_ctx, _parentState)
		var  _prevctx: ExprContext = _localctx
		var _startState: Int = 30
		try enterRecursionRule(_localctx, 30, CollyParser.RULE_expr, _p)
		var _la: Int = 0
		defer {
	    		try! unrollRecursionContexts(_parentctx)
	    }
		do {
			var _alt: Int
			try enterOuterAlt(_localctx, 1)
			setState(516)
			try _errHandler.sync(self)
			switch(try getInterpreter().adaptivePredict(_input,80, _ctx)) {
			case 1:
				_localctx = FunctionCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx

				setState(469)
				try function_call()

				break
			case 2:
				_localctx = ClosureExpressionContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(470)
				try match(CollyParser.COLON)
				setState(474)
				try _errHandler.sync(self)
				_la = try _input.LA(1)
				while (//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NEWLINE
				      return testSet
				 }()) {
					setState(471)
					try match(CollyParser.NEWLINE)


					setState(476)
					try _errHandler.sync(self)
					_la = try _input.LA(1)
				}
				setState(477)
				try closure()

				break
			case 3:
				_localctx = ParenthesisedExpressionContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(478)
				try match(CollyParser.LP)
				setState(482)
				try _errHandler.sync(self)
				_la = try _input.LA(1)
				while (//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NEWLINE
				      return testSet
				 }()) {
					setState(479)
					try match(CollyParser.NEWLINE)


					setState(484)
					try _errHandler.sync(self)
					_la = try _input.LA(1)
				}
				setState(485)
				try expr(0)
				setState(489)
				try _errHandler.sync(self)
				_la = try _input.LA(1)
				while (//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NEWLINE
				      return testSet
				 }()) {
					setState(486)
					try match(CollyParser.NEWLINE)


					setState(491)
					try _errHandler.sync(self)
					_la = try _input.LA(1)
				}
				setState(492)
				try match(CollyParser.RP)

				break
			case 4:
				_localctx = UnaryPlusMinusContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(494)
				_la = try _input.LA(1)
				if (!(//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.PLUS || _la == CollyParser.MINUS
				      return testSet
				 }())) {
				try _errHandler.recoverInline(self)
				} else {
					try consume()
				}
				setState(495)
				try expr(23)

				break
			case 5:
				_localctx = LogicalAndBinaryNegationContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(496)
				_la = try _input.LA(1)
				if (!(//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NOT || _la == CollyParser.TILDE
				      return testSet
				 }())) {
				try _errHandler.recoverInline(self)
				} else {
					try consume()
				}
				setState(497)
				try expr(22)

				break
			case 6:
				_localctx = ExprThenAssignContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(498)
				try match(CollyParser.ID)
				setState(500)
				try _errHandler.sync(self)
				_la = try _input.LA(1)
				if (//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NEWLINE
				      return testSet
				 }()) {
					setState(499)
					try match(CollyParser.NEWLINE)

				}

				setState(502)
				_la = try _input.LA(1)
				if (!(//closure
				 { () -> Bool in
				      let testSet: Bool = {  () -> Bool in
				   let testArray: [Int] = [_la, CollyParser.MULASSIGN,CollyParser.DIVASSIGN,CollyParser.MODASSIGN,CollyParser.PLUSASSIGN,CollyParser.MINUSASSIGN,CollyParser.LSHIFTASSIGN,CollyParser.RSHIFTASSIGN,CollyParser.ANDASSIGN,CollyParser.XORASSIGN,CollyParser.ORASSIGN]
				    return  Utils.testBitLeftShiftArray(testArray, 0)
				}()
				      return testSet
				 }())) {
				try _errHandler.recoverInline(self)
				} else {
					try consume()
				}
				setState(504)
				try _errHandler.sync(self)
				_la = try _input.LA(1)
				if (//closure
				 { () -> Bool in
				      let testSet: Bool = _la == CollyParser.NEWLINE
				      return testSet
				 }()) {
					setState(503)
					try match(CollyParser.NEWLINE)

				}

				setState(506)
				try expr(11)

				break
			case 7:
				_localctx = RangeCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(507)
				try match(CollyParser.RANGE)

				break
			case 8:
				_localctx = NumberCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(508)
				try match(CollyParser.NUMBER)

				break
			case 9:
				_localctx = BooleanCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(509)
				try match(CollyParser.BOOL)

				break
			case 10:
				_localctx = StringCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(510)
				try string()

				break
			case 11:
				_localctx = IdCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(511)
				try match(CollyParser.ID)

				break
			case 12:
				_localctx = DictionaryCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(512)
				try dictionary()

				break
			case 13:
				_localctx = ArrayCallContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(513)
				try array()

				break
			case 14:
				_localctx = PatternExpressionContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(514)
				try pattern_expr()

				break
			case 15:
				_localctx = PropertyAssignContext(_localctx)
				_ctx = _localctx
				_prevctx = _localctx
				setState(515)
				try property_assign()

				break
			default: break
			}
			_ctx!.stop = try _input.LT(-1)
			setState(655)
			try _errHandler.sync(self)
			_alt = try getInterpreter().adaptivePredict(_input,109,_ctx)
			while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
				if ( _alt==1 ) {
					if _parseListeners != nil {
					   try triggerExitRuleEvent()
					}
					_prevctx = _localctx
					setState(653)
					try _errHandler.sync(self)
					switch(try getInterpreter().adaptivePredict(_input,108, _ctx)) {
					case 1:
						_localctx = DotCallContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(518)
						if (!(precpred(_ctx, 27))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 27)"))
						}
						setState(520)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(519)
							try match(CollyParser.NEWLINE)

						}

						setState(522)
						try match(CollyParser.DOT)
						setState(524)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(523)
							try match(CollyParser.NEWLINE)

						}

						setState(526)
						try expr(28)

						break
					case 2:
						_localctx = MulDivModExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(527)
						if (!(precpred(_ctx, 21))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 21)"))
						}
						setState(529)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(528)
							try match(CollyParser.NEWLINE)

						}

						setState(531)
						_la = try _input.LA(1)
						if (!(//closure
						 { () -> Bool in
						      let testSet: Bool = {  () -> Bool in
						   let testArray: [Int] = [_la, CollyParser.MUL,CollyParser.DIV,CollyParser.MOD]
						    return  Utils.testBitLeftShiftArray(testArray, 0)
						}()
						      return testSet
						 }())) {
						try _errHandler.recoverInline(self)
						} else {
							try consume()
						}
						setState(533)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(532)
							try match(CollyParser.NEWLINE)

						}

						setState(535)
						try expr(22)

						break
					case 3:
						_localctx = SumExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(536)
						if (!(precpred(_ctx, 20))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 20)"))
						}
						setState(538)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(537)
							try match(CollyParser.NEWLINE)

						}

						setState(540)
						_la = try _input.LA(1)
						if (!(//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.PLUS || _la == CollyParser.MINUS
						      return testSet
						 }())) {
						try _errHandler.recoverInline(self)
						} else {
							try consume()
						}
						setState(542)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(541)
							try match(CollyParser.NEWLINE)

						}

						setState(544)
						try expr(21)

						break
					case 4:
						_localctx = BinaryShiftContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(545)
						if (!(precpred(_ctx, 19))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 19)"))
						}
						setState(547)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(546)
							try match(CollyParser.NEWLINE)

						}

						setState(549)
						_la = try _input.LA(1)
						if (!(//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.LSHIFT || _la == CollyParser.RSHIFT
						      return testSet
						 }())) {
						try _errHandler.recoverInline(self)
						} else {
							try consume()
						}
						setState(551)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(550)
							try match(CollyParser.NEWLINE)

						}

						setState(553)
						try expr(20)

						break
					case 5:
						_localctx = LessGreaterExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(554)
						if (!(precpred(_ctx, 18))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 18)"))
						}
						setState(556)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(555)
							try match(CollyParser.NEWLINE)

						}

						setState(558)
						_la = try _input.LA(1)
						if (!(//closure
						 { () -> Bool in
						      let testSet: Bool = {  () -> Bool in
						   let testArray: [Int] = [_la, CollyParser.LESS,CollyParser.LESSEQ,CollyParser.GREATER,CollyParser.GREATEREQ]
						    return  Utils.testBitLeftShiftArray(testArray, 0)
						}()
						      return testSet
						 }())) {
						try _errHandler.recoverInline(self)
						} else {
							try consume()
						}
						setState(560)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(559)
							try match(CollyParser.NEWLINE)

						}

						setState(562)
						try expr(19)

						break
					case 6:
						_localctx = EqualityExpsressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(563)
						if (!(precpred(_ctx, 17))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 17)"))
						}
						setState(565)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(564)
							try match(CollyParser.NEWLINE)

						}

						setState(567)
						_la = try _input.LA(1)
						if (!(//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.EQUAL || _la == CollyParser.NOTEQUAL
						      return testSet
						 }())) {
						try _errHandler.recoverInline(self)
						} else {
							try consume()
						}
						setState(569)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(568)
							try match(CollyParser.NEWLINE)

						}

						setState(571)
						try expr(18)

						break
					case 7:
						_localctx = BinaryAndExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(572)
						if (!(precpred(_ctx, 16))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 16)"))
						}
						setState(574)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(573)
							try match(CollyParser.NEWLINE)

						}

						setState(576)
						try match(CollyParser.AND)
						setState(578)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(577)
							try match(CollyParser.NEWLINE)

						}

						setState(580)
						try expr(17)

						break
					case 8:
						_localctx = BinaryXorExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(581)
						if (!(precpred(_ctx, 15))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 15)"))
						}
						setState(583)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(582)
							try match(CollyParser.NEWLINE)

						}

						setState(585)
						try match(CollyParser.XOR)
						setState(587)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(586)
							try match(CollyParser.NEWLINE)

						}

						setState(589)
						try expr(16)

						break
					case 9:
						_localctx = BinaryOrExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(590)
						if (!(precpred(_ctx, 14))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 14)"))
						}
						setState(592)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(591)
							try match(CollyParser.NEWLINE)

						}

						setState(594)
						try match(CollyParser.OR)
						setState(596)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(595)
							try match(CollyParser.NEWLINE)

						}

						setState(598)
						try expr(15)

						break
					case 10:
						_localctx = BooleanAndExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(599)
						if (!(precpred(_ctx, 13))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 13)"))
						}
						setState(601)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(600)
							try match(CollyParser.NEWLINE)

						}

						setState(603)
						try match(CollyParser.BOOL_AND)
						setState(605)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(604)
							try match(CollyParser.NEWLINE)

						}

						setState(607)
						try expr(14)

						break
					case 11:
						_localctx = BooleanOrExpressionContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(608)
						if (!(precpred(_ctx, 12))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 12)"))
						}
						setState(610)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(609)
							try match(CollyParser.NEWLINE)

						}

						setState(612)
						try match(CollyParser.BOOL_OR)
						setState(614)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						if (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(613)
							try match(CollyParser.NEWLINE)

						}

						setState(616)
						try expr(13)

						break
					case 12:
						_localctx = MethodCallContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(617)
						if (!(precpred(_ctx, 25))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 25)"))
						}
						setState(621)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						while (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(618)
							try match(CollyParser.NEWLINE)


							setState(623)
							try _errHandler.sync(self)
							_la = try _input.LA(1)
						}
						setState(624)
						try match(CollyParser.DCOLON)
						setState(628)
						try _errHandler.sync(self)
						_la = try _input.LA(1)
						while (//closure
						 { () -> Bool in
						      let testSet: Bool = _la == CollyParser.NEWLINE
						      return testSet
						 }()) {
							setState(625)
							try match(CollyParser.NEWLINE)


							setState(630)
							try _errHandler.sync(self)
							_la = try _input.LA(1)
						}
						setState(631)
						try closure()

						break
					case 13:
						_localctx = SubscriptCallContext(  ExprContext(_parentctx, _parentState))
						try pushNewRecursionContext(_localctx, _startState, CollyParser.RULE_expr)
						setState(632)
						if (!(precpred(_ctx, 5))) {
						    throw try ANTLRException.recognition(e:FailedPredicateException(self, "precpred(_ctx, 5)"))
						}
						setState(649); 
						try _errHandler.sync(self)
						_alt = 1;
						repeat {
							switch (_alt) {
							case 1:
								setState(633)
								try match(CollyParser.LSP)
								setState(637)
								try _errHandler.sync(self)
								_la = try _input.LA(1)
								while (//closure
								 { () -> Bool in
								      let testSet: Bool = _la == CollyParser.NEWLINE
								      return testSet
								 }()) {
									setState(634)
									try match(CollyParser.NEWLINE)


									setState(639)
									try _errHandler.sync(self)
									_la = try _input.LA(1)
								}
								setState(640)
								try expr(0)
								setState(644)
								try _errHandler.sync(self)
								_la = try _input.LA(1)
								while (//closure
								 { () -> Bool in
								      let testSet: Bool = _la == CollyParser.NEWLINE
								      return testSet
								 }()) {
									setState(641)
									try match(CollyParser.NEWLINE)


									setState(646)
									try _errHandler.sync(self)
									_la = try _input.LA(1)
								}
								setState(647)
								try match(CollyParser.RSP)


								break
							default:
								throw try ANTLRException.recognition(e: NoViableAltException(self))
							}
							setState(651); 
							try _errHandler.sync(self)
							_alt = try getInterpreter().adaptivePredict(_input,107,_ctx)
						} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)

						break
					default: break
					}
			 
				}
				setState(657)
				try _errHandler.sync(self)
				_alt = try getInterpreter().adaptivePredict(_input,109,_ctx)
			}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx;
	}
	open class StringContext:ParserRuleContext {
		open func QUOTE() -> TerminalNode? { return getToken(CollyParser.QUOTE, 0) }
		open func OUT_QUOTE() -> TerminalNode? { return getToken(CollyParser.OUT_QUOTE, 0) }
		open func ANYTHING() -> Array<TerminalNode> { return getTokens(CollyParser.ANYTHING) }
		open func ANYTHING(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.ANYTHING, i)
		}
		open func L_CURLY() -> Array<TerminalNode> { return getTokens(CollyParser.L_CURLY) }
		open func L_CURLY(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.L_CURLY, i)
		}
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func R_CURLY() -> Array<TerminalNode> { return getTokens(CollyParser.R_CURLY) }
		open func R_CURLY(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.R_CURLY, i)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_string }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterString(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitString(self)
			}
		}
	}
	@discardableResult
	open func string() throws -> StringContext {
		var _localctx: StringContext = StringContext(_ctx, getState())
		try enterRule(_localctx, 32, CollyParser.RULE_string)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(658)
		 	try match(CollyParser.QUOTE)
		 	setState(678)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,113,_ctx)
		 	while (_alt != 1 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1+1 ) {
		 			setState(676)
		 			try _errHandler.sync(self)
		 			switch (try _input.LA(1)) {
		 			case CollyParser.ANYTHING:
		 				setState(659)
		 				try match(CollyParser.ANYTHING)

		 				break

		 			case CollyParser.L_CURLY:
		 				setState(660)
		 				try match(CollyParser.L_CURLY)
		 				setState(664)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 				while (//closure
		 				 { () -> Bool in
		 				      let testSet: Bool = _la == CollyParser.NEWLINE
		 				      return testSet
		 				 }()) {
		 					setState(661)
		 					try match(CollyParser.NEWLINE)


		 					setState(666)
		 					try _errHandler.sync(self)
		 					_la = try _input.LA(1)
		 				}
		 				setState(667)
		 				try expr(0)
		 				setState(671)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 				while (//closure
		 				 { () -> Bool in
		 				      let testSet: Bool = _la == CollyParser.NEWLINE
		 				      return testSet
		 				 }()) {
		 					setState(668)
		 					try match(CollyParser.NEWLINE)


		 					setState(673)
		 					try _errHandler.sync(self)
		 					_la = try _input.LA(1)
		 				}
		 				setState(674)
		 				try match(CollyParser.R_CURLY)

		 				break
		 			default:
		 				throw try ANTLRException.recognition(e: NoViableAltException(self))
		 			}
		 	 
		 		}
		 		setState(680)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,113,_ctx)
		 	}
		 	setState(681)
		 	try match(CollyParser.OUT_QUOTE)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class ClosureContext:ParserRuleContext {
		open func LP() -> TerminalNode? { return getToken(CollyParser.LP, 0) }
		open func RP() -> TerminalNode? { return getToken(CollyParser.RP, 0) }
		open func SEMICOLON() -> TerminalNode? { return getToken(CollyParser.SEMICOLON, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func func_params() -> Array<Func_paramsContext> {
			return getRuleContexts(Func_paramsContext.self)
		}
		open func func_params(_ i: Int) -> Func_paramsContext? {
			return getRuleContext(Func_paramsContext.self,i)
		}
		open func stat() -> Array<StatContext> {
			return getRuleContexts(StatContext.self)
		}
		open func stat(_ i: Int) -> StatContext? {
			return getRuleContext(StatContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_closure }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterClosure(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitClosure(self)
			}
		}
	}
	@discardableResult
	open func closure() throws -> ClosureContext {
		var _localctx: ClosureContext = ClosureContext(_ctx, getState())
		try enterRule(_localctx, 34, CollyParser.RULE_closure)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(683)
		 	try match(CollyParser.LP)
		 	setState(687)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,114,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(684)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(689)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,114,_ctx)
		 	}
		 	setState(693)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,115,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(690)
		 			try func_params()

		 	 
		 		}
		 		setState(695)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,115,_ctx)
		 	}
		 	setState(699)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(696)
		 		try match(CollyParser.NEWLINE)


		 		setState(701)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(702)
		 	try match(CollyParser.RP)
		 	setState(706)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,117,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(703)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(708)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,117,_ctx)
		 	}
		 	setState(716); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(710)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			if (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(709)
		 				try match(CollyParser.NEWLINE)

		 			}

		 			setState(712)
		 			try stat()
		 			setState(714)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,119,_ctx)) {
		 			case 1:
		 				setState(713)
		 				try match(CollyParser.NEWLINE)

		 				break
		 			default: break
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(718); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,120,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(723)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(720)
		 		try match(CollyParser.NEWLINE)


		 		setState(725)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(726)
		 	try match(CollyParser.SEMICOLON)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Function_callContext:ParserRuleContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func LP() -> TerminalNode? { return getToken(CollyParser.LP, 0) }
		open func RP() -> TerminalNode? { return getToken(CollyParser.RP, 0) }
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_function_call }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterFunction_call(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitFunction_call(self)
			}
		}
	}
	@discardableResult
	open func function_call() throws -> Function_callContext {
		var _localctx: Function_callContext = Function_callContext(_ctx, getState())
		try enterRule(_localctx, 36, CollyParser.RULE_function_call)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(728)
		 	try match(CollyParser.ID)
		 	setState(729)
		 	try match(CollyParser.LP)
		 	setState(745)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      var testSet: Bool = {  () -> Bool in
		 	   let testArray: [Int] = [_la, CollyParser.LSP,CollyParser.LP,CollyParser.VL,CollyParser.COLON,CollyParser.QUOTE,CollyParser.RANGE,CollyParser.NUMBER,CollyParser.NOT,CollyParser.TILDE,CollyParser.PLUS,CollyParser.MINUS,CollyParser.BOOL,CollyParser.ID]
		 	    return  Utils.testBitLeftShiftArray(testArray, 0)
		 	}()
		 	          testSet = testSet || {  () -> Bool in
		 	             let testArray: [Int] = [_la, CollyParser.NEWLINE,CollyParser.EVENT,CollyParser.BARLINE,CollyParser.PAT_L_ANGL_BRCKT,CollyParser.PAT_L_CURLY,CollyParser.PAT_WS]
		 	              return  Utils.testBitLeftShiftArray(testArray, 64)
		 	          }()
		 	      return testSet
		 	 }()) {
		 		setState(733)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = _la == CollyParser.NEWLINE
		 		      return testSet
		 		 }()) {
		 			setState(730)
		 			try match(CollyParser.NEWLINE)


		 			setState(735)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}
		 		setState(736)
		 		try expr(0)
		 		setState(740)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,123,_ctx)
		 		while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 			if ( _alt==1 ) {
		 				setState(737)
		 				try match(CollyParser.NEWLINE)

		 		 
		 			}
		 			setState(742)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,123,_ctx)
		 		}


		 		setState(747)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(748)
		 	try match(CollyParser.RP)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class ArrayContext:ParserRuleContext {
		open func LSP() -> TerminalNode? { return getToken(CollyParser.LSP, 0) }
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func RSP() -> TerminalNode? { return getToken(CollyParser.RSP, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_array }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterArray(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitArray(self)
			}
		}
	}
	@discardableResult
	open func array() throws -> ArrayContext {
		var _localctx: ArrayContext = ArrayContext(_ctx, getState())
		try enterRule(_localctx, 38, CollyParser.RULE_array)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(750)
		 	try match(CollyParser.LSP)
		 	setState(754)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(751)
		 		try match(CollyParser.NEWLINE)


		 		setState(756)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(757)
		 	try expr(0)
		 	setState(767)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,127,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(761)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			while (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(758)
		 				try match(CollyParser.NEWLINE)


		 				setState(763)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 			}
		 			setState(764)
		 			try expr(0)

		 	 
		 		}
		 		setState(769)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,127,_ctx)
		 	}
		 	setState(773)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(770)
		 		try match(CollyParser.NEWLINE)


		 		setState(775)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(776)
		 	try match(CollyParser.RSP)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class DictionaryContext:ParserRuleContext {
		open func LSP() -> TerminalNode? { return getToken(CollyParser.LSP, 0) }
		open func RSP() -> TerminalNode? { return getToken(CollyParser.RSP, 0) }
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open func key_value_pair() -> Array<Key_value_pairContext> {
			return getRuleContexts(Key_value_pairContext.self)
		}
		open func key_value_pair(_ i: Int) -> Key_value_pairContext? {
			return getRuleContext(Key_value_pairContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_dictionary }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterDictionary(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitDictionary(self)
			}
		}
	}
	@discardableResult
	open func dictionary() throws -> DictionaryContext {
		var _localctx: DictionaryContext = DictionaryContext(_ctx, getState())
		try enterRule(_localctx, 40, CollyParser.RULE_dictionary)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(778)
		 	try match(CollyParser.LSP)
		 	setState(782)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,129,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(779)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(784)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,129,_ctx)
		 	}
		 	setState(798); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(788)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			while (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(785)
		 				try match(CollyParser.NEWLINE)


		 				setState(790)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 			}
		 			setState(791)
		 			try key_value_pair()
		 			setState(795)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,131,_ctx)
		 			while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 				if ( _alt==1 ) {
		 					setState(792)
		 					try match(CollyParser.NEWLINE)

		 			 
		 				}
		 				setState(797)
		 				try _errHandler.sync(self)
		 				_alt = try getInterpreter().adaptivePredict(_input,131,_ctx)
		 			}


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(800); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,132,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(805)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(802)
		 		try match(CollyParser.NEWLINE)


		 		setState(807)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(808)
		 	try match(CollyParser.RSP)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Key_value_pairContext:ParserRuleContext {
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func COLON() -> TerminalNode? { return getToken(CollyParser.COLON, 0) }
		open func expr() -> ExprContext? {
			return getRuleContext(ExprContext.self,0)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_key_value_pair }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterKey_value_pair(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitKey_value_pair(self)
			}
		}
	}
	@discardableResult
	open func key_value_pair() throws -> Key_value_pairContext {
		var _localctx: Key_value_pairContext = Key_value_pairContext(_ctx, getState())
		try enterRule(_localctx, 42, CollyParser.RULE_key_value_pair)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(810)
		 	try match(CollyParser.ID)
		 	setState(814)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(811)
		 		try match(CollyParser.NEWLINE)


		 		setState(816)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(817)
		 	try match(CollyParser.COLON)
		 	setState(821)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(818)
		 		try match(CollyParser.NEWLINE)


		 		setState(823)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(824)
		 	try expr(0)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Property_assignContext:ParserRuleContext {
		open func dictionary() -> DictionaryContext? {
			return getRuleContext(DictionaryContext.self,0)
		}
		open func ID() -> TerminalNode? { return getToken(CollyParser.ID, 0) }
		open func pattern() -> PatternContext? {
			return getRuleContext(PatternContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_property_assign }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterProperty_assign(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitProperty_assign(self)
			}
		}
	}
	@discardableResult
	open func property_assign() throws -> Property_assignContext {
		var _localctx: Property_assignContext = Property_assignContext(_ctx, getState())
		try enterRule(_localctx, 44, CollyParser.RULE_property_assign)
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(828)
		 	try _errHandler.sync(self)
		 	switch (try _input.LA(1)) {
		 	case CollyParser.ID:
		 		setState(826)
		 		try match(CollyParser.ID)

		 		break
		 	case CollyParser.VL:fallthrough
		 	case CollyParser.BARLINE:fallthrough
		 	case CollyParser.PAT_WS:
		 		setState(827)
		 		try pattern()

		 		break
		 	default:
		 		throw try ANTLRException.recognition(e: NoViableAltException(self))
		 	}
		 	setState(830)
		 	try dictionary()

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Pattern_exprContext:ParserRuleContext {
		open func pattern() -> PatternContext? {
			return getRuleContext(PatternContext.self,0)
		}
		open func bar() -> BarContext? {
			return getRuleContext(BarContext.self,0)
		}
		open func EVENT() -> TerminalNode? { return getToken(CollyParser.EVENT, 0) }
		open func events_group() -> Events_groupContext? {
			return getRuleContext(Events_groupContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_pattern_expr }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPattern_expr(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPattern_expr(self)
			}
		}
	}
	@discardableResult
	open func pattern_expr() throws -> Pattern_exprContext {
		var _localctx: Pattern_exprContext = Pattern_exprContext(_ctx, getState())
		try enterRule(_localctx, 46, CollyParser.RULE_pattern_expr)
		defer {
	    		try! exitRule()
	    }
		do {
		 	setState(836)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,137, _ctx)) {
		 	case 1:
		 		try enterOuterAlt(_localctx, 1)
		 		setState(832)
		 		try pattern()

		 		break
		 	case 2:
		 		try enterOuterAlt(_localctx, 2)
		 		setState(833)
		 		try bar()

		 		break
		 	case 3:
		 		try enterOuterAlt(_localctx, 3)
		 		setState(834)
		 		try match(CollyParser.EVENT)

		 		break
		 	case 4:
		 		try enterOuterAlt(_localctx, 4)
		 		setState(835)
		 		try events_group()

		 		break
		 	default: break
		 	}
		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class PatternContext:ParserRuleContext {
		open func PAT_END() -> TerminalNode? { return getToken(CollyParser.PAT_END, 0) }
		open func bar() -> Array<BarContext> {
			return getRuleContexts(BarContext.self)
		}
		open func bar(_ i: Int) -> BarContext? {
			return getRuleContext(BarContext.self,i)
		}
		open func PAT_WS() -> TerminalNode? { return getToken(CollyParser.PAT_WS, 0) }
		open override func getRuleIndex() -> Int { return CollyParser.RULE_pattern }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPattern(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPattern(self)
			}
		}
	}
	@discardableResult
	open func pattern() throws -> PatternContext {
		var _localctx: PatternContext = PatternContext(_ctx, getState())
		try enterRule(_localctx, 48, CollyParser.RULE_pattern)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(839); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(838)
		 			try bar()


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(841); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,138,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(844)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(843)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(846)
		 	try match(CollyParser.PAT_END)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class BarContext:ParserRuleContext {
		open func VL() -> TerminalNode? { return getToken(CollyParser.VL, 0) }
		open func BARLINE() -> TerminalNode? { return getToken(CollyParser.BARLINE, 0) }
		open func bar_property_assign() -> Bar_property_assignContext? {
			return getRuleContext(Bar_property_assignContext.self,0)
		}
		open func events_sequence() -> Events_sequenceContext? {
			return getRuleContext(Events_sequenceContext.self,0)
		}
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open func multibar_func_call() -> Array<Multibar_func_callContext> {
			return getRuleContexts(Multibar_func_callContext.self)
		}
		open func multibar_func_call(_ i: Int) -> Multibar_func_callContext? {
			return getRuleContext(Multibar_func_callContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_bar }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBar(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBar(self)
			}
		}
	}
	@discardableResult
	open func bar() throws -> BarContext {
		var _localctx: BarContext = BarContext(_ctx, getState())
		try enterRule(_localctx, 50, CollyParser.RULE_bar)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(849)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(848)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(854)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,141, _ctx)) {
		 	case 1:
		 		setState(851)
		 		try match(CollyParser.VL)

		 		break
		 	case 2:
		 		setState(852)
		 		try match(CollyParser.BARLINE)

		 		break
		 	case 3:
		 		setState(853)
		 		try bar_property_assign()

		 		break
		 	default: break
		 	}
		 	setState(857)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,142,_ctx)) {
		 	case 1:
		 		setState(856)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(865)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,144, _ctx)) {
		 	case 1:
		 		setState(859)
		 		try events_sequence()

		 		break
		 	case 2:
		 		setState(861); 
		 		try _errHandler.sync(self)
		 		_alt = 1;
		 		repeat {
		 			switch (_alt) {
		 			case 1:
		 				setState(860)
		 				try multibar_func_call()


		 				break
		 			default:
		 				throw try ANTLRException.recognition(e: NoViableAltException(self))
		 			}
		 			setState(863); 
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,143,_ctx)
		 		} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Bar_property_assignContext:ParserRuleContext {
		open func dictionary() -> DictionaryContext? {
			return getRuleContext(DictionaryContext.self,0)
		}
		open func VL() -> TerminalNode? { return getToken(CollyParser.VL, 0) }
		open func BARLINE() -> TerminalNode? { return getToken(CollyParser.BARLINE, 0) }
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_bar_property_assign }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterBar_property_assign(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitBar_property_assign(self)
			}
		}
	}
	@discardableResult
	open func bar_property_assign() throws -> Bar_property_assignContext {
		var _localctx: Bar_property_assignContext = Bar_property_assignContext(_ctx, getState())
		try enterRule(_localctx, 52, CollyParser.RULE_bar_property_assign)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(867)
		 	_la = try _input.LA(1)
		 	if (!(//closure
		 	 { () -> Bool in
		 	      var testSet: Bool = _la == CollyParser.VL
		 	          testSet = testSet || _la == CollyParser.BARLINE
		 	      return testSet
		 	 }())) {
		 	try _errHandler.recoverInline(self)
		 	} else {
		 		try consume()
		 	}
		 	setState(869)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(868)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(871)
		 	try dictionary()
		 	setState(873)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,146,_ctx)) {
		 	case 1:
		 		setState(872)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Multibar_func_callContext:ParserRuleContext {
		open func PAT_L_CURLY() -> TerminalNode? { return getToken(CollyParser.PAT_L_CURLY, 0) }
		open func PAT_COLON() -> TerminalNode? { return getToken(CollyParser.PAT_COLON, 0) }
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func R_CURLY() -> TerminalNode? { return getToken(CollyParser.R_CURLY, 0) }
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open func bar() -> Array<BarContext> {
			return getRuleContexts(BarContext.self)
		}
		open func bar(_ i: Int) -> BarContext? {
			return getRuleContext(BarContext.self,i)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_multibar_func_call }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterMultibar_func_call(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitMultibar_func_call(self)
			}
		}
	}
	@discardableResult
	open func multibar_func_call() throws -> Multibar_func_callContext {
		var _localctx: Multibar_func_callContext = Multibar_func_callContext(_ctx, getState())
		try enterRule(_localctx, 54, CollyParser.RULE_multibar_func_call)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(876)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(875)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(878)
		 	try match(CollyParser.PAT_L_CURLY)
		 	setState(880)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,148,_ctx)) {
		 	case 1:
		 		setState(879)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(883); 
		 	try _errHandler.sync(self)
		 	_alt = 1;
		 	repeat {
		 		switch (_alt) {
		 		case 1:
		 			setState(882)
		 			try bar()


		 			break
		 		default:
		 			throw try ANTLRException.recognition(e: NoViableAltException(self))
		 		}
		 		setState(885); 
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,149,_ctx)
		 	} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)
		 	setState(888)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(887)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(890)
		 	try match(CollyParser.PAT_COLON)
		 	setState(894)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(891)
		 		try match(CollyParser.NEWLINE)


		 		setState(896)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(897)
		 	try expr(0)
		 	setState(901)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,152,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(898)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(903)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,152,_ctx)
		 	}
		 	setState(913)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,154,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(907)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			while (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(904)
		 				try match(CollyParser.NEWLINE)


		 				setState(909)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 			}
		 			setState(910)
		 			try expr(0)

		 	 
		 		}
		 		setState(915)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,154,_ctx)
		 	}
		 	setState(919)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(916)
		 		try match(CollyParser.NEWLINE)


		 		setState(921)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(922)
		 	try match(CollyParser.R_CURLY)
		 	setState(924)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,156,_ctx)) {
		 	case 1:
		 		setState(923)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Events_sequenceContext:ParserRuleContext {
		open func events_group() -> Array<Events_groupContext> {
			return getRuleContexts(Events_groupContext.self)
		}
		open func events_group(_ i: Int) -> Events_groupContext? {
			return getRuleContext(Events_groupContext.self,i)
		}
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_events_sequence }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterEvents_sequence(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitEvents_sequence(self)
			}
		}
	}
	@discardableResult
	open func events_sequence() throws -> Events_sequenceContext {
		var _localctx: Events_sequenceContext = Events_sequenceContext(_ctx, getState())
		try enterRule(_localctx, 56, CollyParser.RULE_events_sequence)
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(927)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,157,_ctx)) {
		 	case 1:
		 		setState(926)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(929)
		 	try events_group()
		 	setState(931)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,158,_ctx)) {
		 	case 1:
		 		setState(930)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(942)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,161,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(934)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,159,_ctx)) {
		 			case 1:
		 				setState(933)
		 				try match(CollyParser.PAT_WS)

		 				break
		 			default: break
		 			}
		 			setState(937)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,160,_ctx)) {
		 			case 1:
		 				setState(936)
		 				try match(CollyParser.PAT_WS)

		 				break
		 			default: break
		 			}
		 			setState(939)
		 			try events_group()

		 	 
		 		}
		 		setState(944)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,161,_ctx)
		 	}
		 	setState(946)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,162,_ctx)) {
		 	case 1:
		 		setState(945)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Events_groupContext:ParserRuleContext {
		open func chord() -> Array<ChordContext> {
			return getRuleContexts(ChordContext.self)
		}
		open func chord(_ i: Int) -> ChordContext? {
			return getRuleContext(ChordContext.self,i)
		}
		open func PAT_WS() -> TerminalNode? { return getToken(CollyParser.PAT_WS, 0) }
		open func EVENT() -> Array<TerminalNode> { return getTokens(CollyParser.EVENT) }
		open func EVENT(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.EVENT, i)
		}
		open func event_property_assign() -> Array<Event_property_assignContext> {
			return getRuleContexts(Event_property_assignContext.self)
		}
		open func event_property_assign(_ i: Int) -> Event_property_assignContext? {
			return getRuleContext(Event_property_assignContext.self,i)
		}
		open func nested_events_group() -> Array<Nested_events_groupContext> {
			return getRuleContexts(Nested_events_groupContext.self)
		}
		open func nested_events_group(_ i: Int) -> Nested_events_groupContext? {
			return getRuleContext(Nested_events_groupContext.self,i)
		}
		open func pat_func_call() -> Array<Pat_func_callContext> {
			return getRuleContexts(Pat_func_callContext.self)
		}
		open func pat_func_call(_ i: Int) -> Pat_func_callContext? {
			return getRuleContext(Pat_func_callContext.self,i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_events_group }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterEvents_group(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitEvents_group(self)
			}
		}
	}
	@discardableResult
	open func events_group() throws -> Events_groupContext {
		var _localctx: Events_groupContext = Events_groupContext(_ctx, getState())
		try enterRule(_localctx, 58, CollyParser.RULE_events_group)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	setState(1020)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,180, _ctx)) {
		 	case 1:
		 		try enterOuterAlt(_localctx, 1)
		 		setState(949)
		 		try _errHandler.sync(self)
		 		switch (try getInterpreter().adaptivePredict(_input,163,_ctx)) {
		 		case 1:
		 			setState(948)
		 			try match(CollyParser.PAT_WS)

		 			break
		 		default: break
		 		}
		 		setState(957)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,165,_ctx)
		 		while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 			if ( _alt==1 ) {
		 				setState(955)
		 				try _errHandler.sync(self)
		 				switch(try getInterpreter().adaptivePredict(_input,164, _ctx)) {
		 				case 1:
		 					setState(951)
		 					try match(CollyParser.EVENT)

		 					break
		 				case 2:
		 					setState(952)
		 					try event_property_assign()

		 					break
		 				case 3:
		 					setState(953)
		 					try nested_events_group()

		 					break
		 				case 4:
		 					setState(954)
		 					try pat_func_call()

		 					break
		 				default: break
		 				}
		 		 
		 			}
		 			setState(959)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,165,_ctx)
		 		}
		 		setState(960)
		 		try chord()
		 		setState(967)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,167,_ctx)
		 		while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 			if ( _alt==1 ) {
		 				setState(965)
		 				try _errHandler.sync(self)
		 				switch(try getInterpreter().adaptivePredict(_input,166, _ctx)) {
		 				case 1:
		 					setState(961)
		 					try match(CollyParser.EVENT)

		 					break
		 				case 2:
		 					setState(962)
		 					try event_property_assign()

		 					break
		 				case 3:
		 					setState(963)
		 					try nested_events_group()

		 					break
		 				case 4:
		 					setState(964)
		 					try pat_func_call()

		 					break
		 				default: break
		 				}
		 		 
		 			}
		 			setState(969)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,167,_ctx)
		 		}

		 		break
		 	case 2:
		 		try enterOuterAlt(_localctx, 2)
		 		setState(971)
		 		try _errHandler.sync(self)
		 		switch (try getInterpreter().adaptivePredict(_input,168,_ctx)) {
		 		case 1:
		 			setState(970)
		 			try match(CollyParser.PAT_WS)

		 			break
		 		default: break
		 		}
		 		setState(979)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      let testSet: Bool = {  () -> Bool in
		 		   let testArray: [Int] = [_la, CollyParser.EVENT,CollyParser.PAT_L_ANGL_BRCKT,CollyParser.PAT_L_CURLY,CollyParser.PAT_WS]
		 		    return  Utils.testBitLeftShiftArray(testArray, 70)
		 		}()
		 		      return testSet
		 		 }()) {
		 			setState(977)
		 			try _errHandler.sync(self)
		 			switch(try getInterpreter().adaptivePredict(_input,169, _ctx)) {
		 			case 1:
		 				setState(973)
		 				try match(CollyParser.EVENT)

		 				break
		 			case 2:
		 				setState(974)
		 				try event_property_assign()

		 				break
		 			case 3:
		 				setState(975)
		 				try chord()

		 				break
		 			case 4:
		 				setState(976)
		 				try pat_func_call()

		 				break
		 			default: break
		 			}

		 			setState(981)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}
		 		setState(982)
		 		try nested_events_group()
		 		setState(989)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,172,_ctx)
		 		while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 			if ( _alt==1 ) {
		 				setState(987)
		 				try _errHandler.sync(self)
		 				switch(try getInterpreter().adaptivePredict(_input,171, _ctx)) {
		 				case 1:
		 					setState(983)
		 					try match(CollyParser.EVENT)

		 					break
		 				case 2:
		 					setState(984)
		 					try event_property_assign()

		 					break
		 				case 3:
		 					setState(985)
		 					try chord()

		 					break
		 				case 4:
		 					setState(986)
		 					try pat_func_call()

		 					break
		 				default: break
		 				}
		 		 
		 			}
		 			setState(991)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,172,_ctx)
		 		}

		 		break
		 	case 3:
		 		try enterOuterAlt(_localctx, 3)
		 		setState(993)
		 		try _errHandler.sync(self)
		 		switch (try getInterpreter().adaptivePredict(_input,173,_ctx)) {
		 		case 1:
		 			setState(992)
		 			try match(CollyParser.PAT_WS)

		 			break
		 		default: break
		 		}
		 		setState(1001)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 		while (//closure
		 		 { () -> Bool in
		 		      var testSet: Bool = _la == CollyParser.LP
		 		          testSet = testSet || _la == CollyParser.EVENT || _la == CollyParser.PAT_L_ANGL_BRCKT
		 		      return testSet
		 		 }()) {
		 			setState(999)
		 			try _errHandler.sync(self)
		 			switch(try getInterpreter().adaptivePredict(_input,174, _ctx)) {
		 			case 1:
		 				setState(995)
		 				try match(CollyParser.EVENT)

		 				break
		 			case 2:
		 				setState(996)
		 				try event_property_assign()

		 				break
		 			case 3:
		 				setState(997)
		 				try chord()

		 				break
		 			case 4:
		 				setState(998)
		 				try nested_events_group()

		 				break
		 			default: break
		 			}

		 			setState(1003)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 		}
		 		setState(1004)
		 		try pat_func_call()
		 		setState(1011)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,177,_ctx)
		 		while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 			if ( _alt==1 ) {
		 				setState(1009)
		 				try _errHandler.sync(self)
		 				switch(try getInterpreter().adaptivePredict(_input,176, _ctx)) {
		 				case 1:
		 					setState(1005)
		 					try match(CollyParser.EVENT)

		 					break
		 				case 2:
		 					setState(1006)
		 					try event_property_assign()

		 					break
		 				case 3:
		 					setState(1007)
		 					try chord()

		 					break
		 				case 4:
		 					setState(1008)
		 					try nested_events_group()

		 					break
		 				default: break
		 				}
		 		 
		 			}
		 			setState(1013)
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,177,_ctx)
		 		}

		 		break
		 	case 4:
		 		try enterOuterAlt(_localctx, 4)
		 		setState(1016); 
		 		try _errHandler.sync(self)
		 		_alt = 1;
		 		repeat {
		 			switch (_alt) {
		 			case 1:
		 				setState(1016)
		 				try _errHandler.sync(self)
		 				switch(try getInterpreter().adaptivePredict(_input,178, _ctx)) {
		 				case 1:
		 					setState(1014)
		 					try match(CollyParser.EVENT)

		 					break
		 				case 2:
		 					setState(1015)
		 					try event_property_assign()

		 					break
		 				default: break
		 				}

		 				break
		 			default:
		 				throw try ANTLRException.recognition(e: NoViableAltException(self))
		 			}
		 			setState(1018); 
		 			try _errHandler.sync(self)
		 			_alt = try getInterpreter().adaptivePredict(_input,179,_ctx)
		 		} while (_alt != 2 && _alt !=  ATN.INVALID_ALT_NUMBER)

		 		break
		 	default: break
		 	}
		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class ChordContext:ParserRuleContext {
		open func PAT_L_ANGL_BRCKT() -> TerminalNode? { return getToken(CollyParser.PAT_L_ANGL_BRCKT, 0) }
		open func events_group() -> Array<Events_groupContext> {
			return getRuleContexts(Events_groupContext.self)
		}
		open func events_group(_ i: Int) -> Events_groupContext? {
			return getRuleContext(Events_groupContext.self,i)
		}
		open func PAT_R_ANGL_BRCKT() -> TerminalNode? { return getToken(CollyParser.PAT_R_ANGL_BRCKT, 0) }
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_chord }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterChord(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitChord(self)
			}
		}
	}
	@discardableResult
	open func chord() throws -> ChordContext {
		var _localctx: ChordContext = ChordContext(_ctx, getState())
		try enterRule(_localctx, 60, CollyParser.RULE_chord)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(1022)
		 	try match(CollyParser.PAT_L_ANGL_BRCKT)
		 	setState(1024)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,181,_ctx)) {
		 	case 1:
		 		setState(1023)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(1026)
		 	try events_group()
		 	setState(1033)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,183,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(1028)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,182,_ctx)) {
		 			case 1:
		 				setState(1027)
		 				try match(CollyParser.PAT_WS)

		 				break
		 			default: break
		 			}
		 			setState(1030)
		 			try events_group()

		 	 
		 		}
		 		setState(1035)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,183,_ctx)
		 	}
		 	setState(1037)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(1036)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(1039)
		 	try match(CollyParser.PAT_R_ANGL_BRCKT)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Nested_events_groupContext:ParserRuleContext {
		open func LP() -> TerminalNode? { return getToken(CollyParser.LP, 0) }
		open func events_group() -> Array<Events_groupContext> {
			return getRuleContexts(Events_groupContext.self)
		}
		open func events_group(_ i: Int) -> Events_groupContext? {
			return getRuleContext(Events_groupContext.self,i)
		}
		open func RP() -> TerminalNode? { return getToken(CollyParser.RP, 0) }
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_nested_events_group }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterNested_events_group(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitNested_events_group(self)
			}
		}
	}
	@discardableResult
	open func nested_events_group() throws -> Nested_events_groupContext {
		var _localctx: Nested_events_groupContext = Nested_events_groupContext(_ctx, getState())
		try enterRule(_localctx, 62, CollyParser.RULE_nested_events_group)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(1041)
		 	try match(CollyParser.LP)
		 	setState(1043)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,185,_ctx)) {
		 	case 1:
		 		setState(1042)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(1045)
		 	try events_group()
		 	setState(1052)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,187,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(1047)
		 			try _errHandler.sync(self)
		 			switch (try getInterpreter().adaptivePredict(_input,186,_ctx)) {
		 			case 1:
		 				setState(1046)
		 				try match(CollyParser.PAT_WS)

		 				break
		 			default: break
		 			}
		 			setState(1049)
		 			try events_group()

		 	 
		 		}
		 		setState(1054)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,187,_ctx)
		 	}
		 	setState(1056)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(1055)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(1058)
		 	try match(CollyParser.RP)

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Event_property_assignContext:ParserRuleContext {
		open func dictionary() -> DictionaryContext? {
			return getRuleContext(DictionaryContext.self,0)
		}
		open func EVENT() -> TerminalNode? { return getToken(CollyParser.EVENT, 0) }
		open func chord() -> ChordContext? {
			return getRuleContext(ChordContext.self,0)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_event_property_assign }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterEvent_property_assign(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitEvent_property_assign(self)
			}
		}
	}
	@discardableResult
	open func event_property_assign() throws -> Event_property_assignContext {
		var _localctx: Event_property_assignContext = Event_property_assignContext(_ctx, getState())
		try enterRule(_localctx, 64, CollyParser.RULE_event_property_assign)
		defer {
	    		try! exitRule()
	    }
		do {
		 	try enterOuterAlt(_localctx, 1)
		 	setState(1062)
		 	try _errHandler.sync(self)
		 	switch (try _input.LA(1)) {
		 	case CollyParser.EVENT:
		 		setState(1060)
		 		try match(CollyParser.EVENT)

		 		break

		 	case CollyParser.PAT_L_ANGL_BRCKT:
		 		setState(1061)
		 		try chord()

		 		break
		 	default:
		 		throw try ANTLRException.recognition(e: NoViableAltException(self))
		 	}
		 	setState(1064)
		 	try dictionary()

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}
	open class Pat_func_callContext:ParserRuleContext {
		open func PAT_L_CURLY() -> TerminalNode? { return getToken(CollyParser.PAT_L_CURLY, 0) }
		open func PAT_COLON() -> TerminalNode? { return getToken(CollyParser.PAT_COLON, 0) }
		open func expr() -> Array<ExprContext> {
			return getRuleContexts(ExprContext.self)
		}
		open func expr(_ i: Int) -> ExprContext? {
			return getRuleContext(ExprContext.self,i)
		}
		open func R_CURLY() -> TerminalNode? { return getToken(CollyParser.R_CURLY, 0) }
		open func events_group() -> Events_groupContext? {
			return getRuleContext(Events_groupContext.self,0)
		}
		open func events_sequence() -> Events_sequenceContext? {
			return getRuleContext(Events_sequenceContext.self,0)
		}
		open func PAT_WS() -> Array<TerminalNode> { return getTokens(CollyParser.PAT_WS) }
		open func PAT_WS(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.PAT_WS, i)
		}
		open func NEWLINE() -> Array<TerminalNode> { return getTokens(CollyParser.NEWLINE) }
		open func NEWLINE(_ i:Int) -> TerminalNode?{
			return getToken(CollyParser.NEWLINE, i)
		}
		open override func getRuleIndex() -> Int { return CollyParser.RULE_pat_func_call }
		override
		open func enterRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).enterPat_func_call(self)
			}
		}
		override
		open func exitRule(_ listener: ParseTreeListener) {
			if listener is CollyParserListener {
			 	(listener as! CollyParserListener).exitPat_func_call(self)
			}
		}
	}
	@discardableResult
	open func pat_func_call() throws -> Pat_func_callContext {
		var _localctx: Pat_func_callContext = Pat_func_callContext(_ctx, getState())
		try enterRule(_localctx, 66, CollyParser.RULE_pat_func_call)
		var _la: Int = 0
		defer {
	    		try! exitRule()
	    }
		do {
			var _alt:Int
		 	try enterOuterAlt(_localctx, 1)
		 	setState(1067)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(1066)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(1069)
		 	try match(CollyParser.PAT_L_CURLY)
		 	setState(1071)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,191,_ctx)) {
		 	case 1:
		 		setState(1070)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}
		 	setState(1075)
		 	try _errHandler.sync(self)
		 	switch(try getInterpreter().adaptivePredict(_input,192, _ctx)) {
		 	case 1:
		 		setState(1073)
		 		try events_group()

		 		break
		 	case 2:
		 		setState(1074)
		 		try events_sequence()

		 		break
		 	default: break
		 	}
		 	setState(1078)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	if (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.PAT_WS
		 	      return testSet
		 	 }()) {
		 		setState(1077)
		 		try match(CollyParser.PAT_WS)

		 	}

		 	setState(1080)
		 	try match(CollyParser.PAT_COLON)
		 	setState(1084)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(1081)
		 		try match(CollyParser.NEWLINE)


		 		setState(1086)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(1087)
		 	try expr(0)
		 	setState(1091)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,195,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(1088)
		 			try match(CollyParser.NEWLINE)

		 	 
		 		}
		 		setState(1093)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,195,_ctx)
		 	}
		 	setState(1103)
		 	try _errHandler.sync(self)
		 	_alt = try getInterpreter().adaptivePredict(_input,197,_ctx)
		 	while (_alt != 2 && _alt != ATN.INVALID_ALT_NUMBER) {
		 		if ( _alt==1 ) {
		 			setState(1097)
		 			try _errHandler.sync(self)
		 			_la = try _input.LA(1)
		 			while (//closure
		 			 { () -> Bool in
		 			      let testSet: Bool = _la == CollyParser.NEWLINE
		 			      return testSet
		 			 }()) {
		 				setState(1094)
		 				try match(CollyParser.NEWLINE)


		 				setState(1099)
		 				try _errHandler.sync(self)
		 				_la = try _input.LA(1)
		 			}
		 			setState(1100)
		 			try expr(0)

		 	 
		 		}
		 		setState(1105)
		 		try _errHandler.sync(self)
		 		_alt = try getInterpreter().adaptivePredict(_input,197,_ctx)
		 	}
		 	setState(1109)
		 	try _errHandler.sync(self)
		 	_la = try _input.LA(1)
		 	while (//closure
		 	 { () -> Bool in
		 	      let testSet: Bool = _la == CollyParser.NEWLINE
		 	      return testSet
		 	 }()) {
		 		setState(1106)
		 		try match(CollyParser.NEWLINE)


		 		setState(1111)
		 		try _errHandler.sync(self)
		 		_la = try _input.LA(1)
		 	}
		 	setState(1112)
		 	try match(CollyParser.R_CURLY)
		 	setState(1114)
		 	try _errHandler.sync(self)
		 	switch (try getInterpreter().adaptivePredict(_input,199,_ctx)) {
		 	case 1:
		 		setState(1113)
		 		try match(CollyParser.PAT_WS)

		 		break
		 	default: break
		 	}

		}
		catch ANTLRException.recognition(let re) {
			_localctx.exception = re
			_errHandler.reportError(self, re)
			try _errHandler.recover(self, re)
		}

		return _localctx
	}

    override
	open func sempred(_ _localctx: RuleContext?, _ ruleIndex: Int,  _ predIndex: Int)throws -> Bool {
		switch (ruleIndex) {
		case  15:
			return try expr_sempred(_localctx?.castdown(ExprContext.self), predIndex)
	    default: return true
		}
	}
	private func expr_sempred(_ _localctx: ExprContext!,  _ predIndex: Int) throws -> Bool {
		switch (predIndex) {
		    case 0:return precpred(_ctx, 27)
		    case 1:return precpred(_ctx, 21)
		    case 2:return precpred(_ctx, 20)
		    case 3:return precpred(_ctx, 19)
		    case 4:return precpred(_ctx, 18)
		    case 5:return precpred(_ctx, 17)
		    case 6:return precpred(_ctx, 16)
		    case 7:return precpred(_ctx, 15)
		    case 8:return precpred(_ctx, 14)
		    case 9:return precpred(_ctx, 13)
		    case 10:return precpred(_ctx, 12)
		    case 11:return precpred(_ctx, 25)
		    case 12:return precpred(_ctx, 5)
		    default: return true
		}
	}

   public static let _serializedATN : String = CollyParserATN().jsonString
   public static let _ATN: ATN = ATNDeserializer().deserializeFromJson(_serializedATN)
}
