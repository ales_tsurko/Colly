lexer grammar CollyLexer;

IMPORT : 'import' ;

LSP     : '[' -> pushMode(DEFAULT_MODE) ;
RSP     : ']' -> popMode                ;
LP      : '('                           ;
RP      : ')'                           ;
// когда '{' вне паттерна, то режим переключается в обычный,
// потому что закрывающая скобка ('}') всегда выходит через
// popMode - нужен какой-то режим на стеке
// в свою очередь popMode нужен, чтобы возвращаться в режим
// грамматики паттерна (когда там использются такие скобки)
L_CURLY   : '{' -> pushMode(DEFAULT_MODE) ;
R_CURLY   : '}' -> popMode                ;
VL        : '|' -> pushMode(PatternLex)   ;
DOLLAR    : '$'                           ;
DCOLON    : '::'                          ;
COLON     : ':'                           ;
SEMICOLON : ';'                           ;
DOT       : '.'                           ;
QUOTE     : '"' -> pushMode(String)       ;
// +--------------------------------------+
// |                                      |
// |                NUMBER                |
// |                                      |
// +--------------------------------------+
RANGE                  : '-'? INT '..' '-'? INT                   ;
NUMBER                 : SIMPLE_NUMBER
                       | SCI_NUMBER
                       | HEX_INT
                       | BIN_INT
                       | OCT_INT
                       ;
fragment SCI_NUMBER    : SIMPLE_NUMBER ([eE] '-'? SIMPLE_NUMBER)? ;
fragment SIMPLE_NUMBER : (FLOAT|INT)                              ;
fragment FLOAT         : DIGIT+ '.' DIGIT*
                       | '.' DIGIT+                               ;
fragment INT           : [0-9] DIGIT*                             ;
// hex
fragment HEX_INT       : '0' [xX] HEX+                            ;
fragment BIN_INT       : '0' [bB] [0-1]+                          ;
// binary
fragment OCT_INT       : '0' [oO] [0-7]+                          ;
// octal
fragment DIGIT         : [0-9]                                    ;
fragment HEX           : [0-9a-fA-F]                              ;

// +--------------------------------------+
// |                                      |
// |              OPERATORS               |
// |                                      |
// +--------------------------------------+
// in precedence order

NOT          : '!'   ; // as unary in logical negation
TILDE        : '~'   ; // bitwise not

MUL          : '*'   ;
DIV          : '/'   ;
MOD          : '%'   ;

PLUS         : '+'   ;
MINUS        : '-'   ;

LSHIFT       : '<<'  ;
RSHIFT       : '>>'  ;

LESS         : '<'   ;
LESSEQ       : '<='  ;
GREATER      : '>'   ;
GREATEREQ    : '>='  ;

EQUAL        : '=='  ;
NOTEQUAL     : '!='  ;

AND          : '&'   ;

XOR          : '^'   ;

OR           : 'bor' ;

BOOL_AND     : '&&' ;

BOOL_OR      : '||'  ;

ASSIGN       : '='   ;
// '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | ' | ='
MULASSIGN    : '*='  ;
DIVASSIGN    : '/='  ;
MODASSIGN    : '%='  ;
PLUSASSIGN   : '+='  ;
MINUSASSIGN  : '-='  ;
LSHIFTASSIGN : '<<=' ;
RSHIFTASSIGN : '>>=' ;
ANDASSIGN    : '&='  ;
XORASSIGN    : '^='  ;
ORASSIGN     : '|='  ;


//
// CONTROL FLOW
//

BOOL      : TRUE|FALSE ;
TRUE      : 'true'     ;
FALSE     : 'false'    ;
IF        : 'if'       ;
ELSE      : 'else'     ;
SWITCH    : 'switch'   ;
CASE      : 'case'     ;
DEFAULT   : 'default'  ;
WHILE     : 'while'    ;
DO        : 'do'       ;
UNTIL     : 'until'    ;
/*FOR       : 'for'      ;*/
/*IN        : 'in'       ;*/
RETURN    : 'return'   ;
BREAK     : 'break'    ;
CONTINUE  : 'continue' ;

ID               : LETTER (LETTER|DIGIT)*    ;
LETTER           : [a-zA-Z_]                 ;

NEWLINE          : '\r'? '\n'                ;
WS               : [ \t]+ -> skip            ;
LINE_ESCAPE      : '\\' '\r'? '\n' -> skip   ;
COMMENT          : '#' ~[\r\n]* -> skip      ;

//
// STRING
//

mode String ;

OUT_QUOTE        : '"' -> popMode                               ;
STR_L_CURLY      : '{' -> type(L_CURLY), pushMode(DEFAULT_MODE) ;
ANYTHING         : (ESC|~[{}"])+                                ;
fragment ESC     : '\\' ([btnr{}"\\]|UNICODE)                   ;
fragment UNICODE : 'u' HEX HEX HEX HEX                          ;

/////////////
// PATTERN
/////////////

mode PatternLex ;

EVENT                           : PAUSE|NOTE              ;
NOTE                            : PITCH RHYTHM?           ;
// pitch
fragment PITCH                  : ALTERATION?
                                  PITCH_LETTER
                                  (OCTAVE_DOWN_TRANSPOSER
                                  |OCTAVE_UP_TRANSPOSER)* ;
fragment PITCH_LETTER           : [a-gA-G]                ;
fragment OCTAVE_UP_TRANSPOSER   : '\''                    ;
fragment OCTAVE_DOWN_TRANSPOSER : ','                     ;
fragment ALTERATION             : ('+'|'-')               ;
// rhythm
fragment PAUSE                  : '-' RHYTHM              ;
fragment RHYTHM                 : (PAT_INT|PAT_FLOAT)     ;
fragment PAT_INT                : ([0-9]|[1-9] [0-9]+)    ;
fragment PAT_FLOAT              : PAT_INT '.' [0-9]*
				                        | '.' [0-9]+              ;

PAT_END          : ';' -> popMode                           ;
BARLINE          : '|'                                      ;
PAT_LP           : '(' -> type(LP)                          ;
PAT_RP           : ')' -> type(RP)                          ;
PAT_LSP          : '[' -> type(LSP), pushMode(DEFAULT_MODE) ;
PAT_RSP          : ']' -> type(RSP), popMode                ;
PAT_L_ANGL_BRCKT : '<'                                      ;
PAT_R_ANGL_BRCKT : '>'                                      ;
PAT_L_CURLY      : '{'                                      ;
// когда '{' в паттерне, то режим грамматики не переключается
// токен ':' должен быть в двух режимах:
// в обычном, он не переключает грамматику,
// в паттернах, он включает обычный режим (используется в вызове функций)
PAT_COLON       : ':' -> pushMode(DEFAULT_MODE) ;
PAT_WS          : [ \t]+                        ;

PAT_NEWLINE     : '\r'? '\n' -> skip            ;
PAT_COMMENT     : '#' ~[\r\n]* -> skip          ;
PAT_LINE_ESCAPE : '\\' '\r'? '\n' -> skip       ;
