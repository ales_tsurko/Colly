// Generated from CollyLexer.g4 by ANTLR 4.5.3
import Antlr4

open class CollyLexer: Lexer {
	internal static var _decisionToDFA: [DFA] = {
          var decisionToDFA = [DFA]()
          let length = CollyLexer._ATN.getNumberOfDecisions()
          for i in 0..<length {
          	    decisionToDFA.append(DFA(CollyLexer._ATN.getDecisionState(i)!, i))
          }
           return decisionToDFA
     }()

	internal static let _sharedContextCache:PredictionContextCache = PredictionContextCache()
	public static let IMPORT=1, LSP=2, RSP=3, LP=4, RP=5, L_CURLY=6, R_CURLY=7, 
                   VL=8, DOLLAR=9, DCOLON=10, COLON=11, SEMICOLON=12, DOT=13, 
                   QUOTE=14, RANGE=15, NUMBER=16, NOT=17, TILDE=18, MUL=19, 
                   DIV=20, MOD=21, PLUS=22, MINUS=23, LSHIFT=24, RSHIFT=25, 
                   LESS=26, LESSEQ=27, GREATER=28, GREATEREQ=29, EQUAL=30, 
                   NOTEQUAL=31, AND=32, XOR=33, OR=34, BOOL_AND=35, BOOL_OR=36, 
                   ASSIGN=37, MULASSIGN=38, DIVASSIGN=39, MODASSIGN=40, 
                   PLUSASSIGN=41, MINUSASSIGN=42, LSHIFTASSIGN=43, RSHIFTASSIGN=44, 
                   ANDASSIGN=45, XORASSIGN=46, ORASSIGN=47, BOOL=48, TRUE=49, 
                   FALSE=50, IF=51, ELSE=52, SWITCH=53, CASE=54, DEFAULT=55, 
                   WHILE=56, DO=57, UNTIL=58, RETURN=59, BREAK=60, CONTINUE=61, 
                   ID=62, LETTER=63, NEWLINE=64, WS=65, LINE_ESCAPE=66, 
                   COMMENT=67, OUT_QUOTE=68, ANYTHING=69, EVENT=70, NOTE=71, 
                   PAT_END=72, BARLINE=73, PAT_L_ANGL_BRCKT=74, PAT_R_ANGL_BRCKT=75, 
                   PAT_L_CURLY=76, PAT_COLON=77, PAT_WS=78, PAT_NEWLINE=79, 
                   PAT_COMMENT=80, PAT_LINE_ESCAPE=81
	public static let String: Int = 1;
	public static let PatternLex: Int = 2;
	public static let modeNames: [String] = [
		"DEFAULT_MODE", "String", "PatternLex"
	]

	public static let ruleNames: [String] = [
		"IMPORT", "LSP", "RSP", "LP", "RP", "L_CURLY", "R_CURLY", "VL", "DOLLAR", 
		"DCOLON", "COLON", "SEMICOLON", "DOT", "QUOTE", "RANGE", "NUMBER", "SCI_NUMBER", 
		"SIMPLE_NUMBER", "FLOAT", "INT", "HEX_INT", "BIN_INT", "OCT_INT", "DIGIT", 
		"HEX", "NOT", "TILDE", "MUL", "DIV", "MOD", "PLUS", "MINUS", "LSHIFT", 
		"RSHIFT", "LESS", "LESSEQ", "GREATER", "GREATEREQ", "EQUAL", "NOTEQUAL", 
		"AND", "XOR", "OR", "BOOL_AND", "BOOL_OR", "ASSIGN", "MULASSIGN", "DIVASSIGN", 
		"MODASSIGN", "PLUSASSIGN", "MINUSASSIGN", "LSHIFTASSIGN", "RSHIFTASSIGN", 
		"ANDASSIGN", "XORASSIGN", "ORASSIGN", "BOOL", "TRUE", "FALSE", "IF", "ELSE", 
		"SWITCH", "CASE", "DEFAULT", "WHILE", "DO", "UNTIL", "RETURN", "BREAK", 
		"CONTINUE", "ID", "LETTER", "NEWLINE", "WS", "LINE_ESCAPE", "COMMENT", 
		"OUT_QUOTE", "STR_L_CURLY", "ANYTHING", "ESC", "UNICODE", "EVENT", "NOTE", 
		"PITCH", "PITCH_LETTER", "OCTAVE_UP_TRANSPOSER", "OCTAVE_DOWN_TRANSPOSER", 
		"ALTERATION", "PAUSE", "RHYTHM", "PAT_INT", "PAT_FLOAT", "PAT_END", "BARLINE", 
		"PAT_LP", "PAT_RP", "PAT_LSP", "PAT_RSP", "PAT_L_ANGL_BRCKT", "PAT_R_ANGL_BRCKT", 
		"PAT_L_CURLY", "PAT_COLON", "PAT_WS", "PAT_NEWLINE", "PAT_COMMENT", "PAT_LINE_ESCAPE"
	]

	private static let _LITERAL_NAMES: [String?] = [
		nil, "'import'", "'['", nil, nil, nil, nil, "'}'", nil, "'$'", "'::'", 
		nil, nil, "'.'", nil, nil, nil, "'!'", "'~'", "'*'", "'/'", "'%'", "'+'", 
		"'-'", "'<<'", "'>>'", nil, "'<='", nil, "'>='", "'=='", "'!='", "'&'", 
		"'^'", "'bor'", "'&&'", "'||'", "'='", "'*='", "'/='", "'%='", "'+='", 
		"'-='", "'<<='", "'>>='", "'&='", "'^='", "'|='", nil, "'true'", "'false'", 
		"'if'", "'else'", "'switch'", "'case'", "'default'", "'while'", "'do'", 
		"'until'", "'return'", "'break'", "'continue'"
	]
	private static let _SYMBOLIC_NAMES: [String?] = [
		nil, "IMPORT", "LSP", "RSP", "LP", "RP", "L_CURLY", "R_CURLY", "VL", "DOLLAR", 
		"DCOLON", "COLON", "SEMICOLON", "DOT", "QUOTE", "RANGE", "NUMBER", "NOT", 
		"TILDE", "MUL", "DIV", "MOD", "PLUS", "MINUS", "LSHIFT", "RSHIFT", "LESS", 
		"LESSEQ", "GREATER", "GREATEREQ", "EQUAL", "NOTEQUAL", "AND", "XOR", "OR", 
		"BOOL_AND", "BOOL_OR", "ASSIGN", "MULASSIGN", "DIVASSIGN", "MODASSIGN", 
		"PLUSASSIGN", "MINUSASSIGN", "LSHIFTASSIGN", "RSHIFTASSIGN", "ANDASSIGN", 
		"XORASSIGN", "ORASSIGN", "BOOL", "TRUE", "FALSE", "IF", "ELSE", "SWITCH", 
		"CASE", "DEFAULT", "WHILE", "DO", "UNTIL", "RETURN", "BREAK", "CONTINUE", 
		"ID", "LETTER", "NEWLINE", "WS", "LINE_ESCAPE", "COMMENT", "OUT_QUOTE", 
		"ANYTHING", "EVENT", "NOTE", "PAT_END", "BARLINE", "PAT_L_ANGL_BRCKT", 
		"PAT_R_ANGL_BRCKT", "PAT_L_CURLY", "PAT_COLON", "PAT_WS", "PAT_NEWLINE", 
		"PAT_COMMENT", "PAT_LINE_ESCAPE"
	]
	public static let VOCABULARY: Vocabulary = Vocabulary(_LITERAL_NAMES, _SYMBOLIC_NAMES)

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	//@Deprecated
	public let tokenNames: [String?]? = {
	    let length = _SYMBOLIC_NAMES.count
		var tokenNames = Array<String?>(repeating: nil, count: length)
		for i in 0..<length {
			var name = VOCABULARY.getLiteralName(i)
			if name == nil {
				name = VOCABULARY.getSymbolicName(i)
			}
			if name == nil {
				name = "<INVALID>"
			}
			tokenNames[i] = name
		}
		return tokenNames
	}()

	override
	open func getTokenNames() -> [String?]? {
		return tokenNames
	}

    open override func getVocabulary() -> Vocabulary {
        return CollyLexer.VOCABULARY
    }

	public override init(_ input: CharStream) {
	    RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION)
		super.init(input)
		_interp = LexerATNSimulator(self, CollyLexer._ATN, CollyLexer._decisionToDFA, CollyLexer._sharedContextCache)
	}

	override
	open func getGrammarFileName() -> String { return "CollyLexer.g4" }

    override
	open func getRuleNames() -> [String] { return CollyLexer.ruleNames }

	override
	open func getSerializedATN() -> String { return CollyLexer._serializedATN }

	override
	open func getModeNames() -> [String] { return CollyLexer.modeNames }

	override
	open func getATN() -> ATN { return CollyLexer._ATN }

    public static let _serializedATN: String = CollyLexerATN().jsonString
	public static let _ATN: ATN = ATNDeserializer().deserializeFromJson(_serializedATN)

}
