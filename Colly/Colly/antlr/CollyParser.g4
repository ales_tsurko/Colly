parser grammar CollyParser;
options { tokenVocab=CollyLexer; }

score   : end_stat+ EOF? ;

end_stat : IMPORT ID (NEWLINE|EOF)
         | stat (NEWLINE|EOF)
         | NEWLINE
         ;

// expression should be at higher precedence than assignment,
// because we are calculate first and then assign the value
stat: expr             # expressionStatement
    | assignment       # assignStatement
    | func_def         # functionDefinition
    | if_stat          # ifStatement
    | switch_stat      # switchStatement
    | while_stat       # whileStatement
    | do_until_stat    # doUntilStatement
    /*| for_in           # forIn*/
    | return_stat      # returnStatement
    | BREAK            # breakStatement
    | CONTINUE         # continueStatement
    ;

// return
return_stat : RETURN NEWLINE* (NEWLINE? stat NEWLINE?)?   ;

// function definition
func_def    : COLON NEWLINE* ID NEWLINE*
              LP NEWLINE* func_params* NEWLINE* RP
              NEWLINE* (NEWLINE? stat NEWLINE?)+ NEWLINE*
              SEMICOLON                                   ;
func_params : (NEWLINE? (ID|assignment) NEWLINE?)         ;

// if
if_stat : IF NEWLINE* expr NEWLINE*
         (NEWLINE? stat NEWLINE?)+ NEWLINE*
         else_if*
         if_else?
         SEMICOLON                                         ;
else_if : ELSE IF NEWLINE* expr NEWLINE*
         (NEWLINE? stat NEWLINE?)+ NEWLINE*                ;
if_else : ELSE NEWLINE* (NEWLINE? stat NEWLINE?)+ NEWLINE* ;

// switch
switch_stat    : SWITCH NEWLINE* expr NEWLINE*
                 (switch_case NEWLINE*)+
                 switch_default NEWLINE*
                 SEMICOLON?                                   ;
switch_case    : CASE NEWLINE* expr NEWLINE*
                 (NEWLINE? stat NEWLINE?)+ NEWLINE* SEMICOLON ;
switch_default : DEFAULT NEWLINE*
                 (NEWLINE? stat NEWLINE?)+ NEWLINE* SEMICOLON ;

// while
while_stat    : WHILE NEWLINE* expr NEWLINE*
                (NEWLINE? stat NEWLINE?)+ NEWLINE* SEMICOLON ;

// do-while
do_until_stat : DO NEWLINE*
                (NEWLINE? stat NEWLINE?)+ NEWLINE* 
                UNTIL NEWLINE* expr SEMICOLON?               ;

// for-in
// не нужен, благодаря closures (см. functions.colly для примера)
/*for_in: FOR NEWLINE* ID NEWLINE* (ID NEWLINE*)? IN NEWLINE* expr NEWLINE* (stat NEWLINE?)+ NEWLINE* SEMICOLON ;*/

assignment: ID NEWLINE? ASSIGN NEWLINE? expr      # expressionAssignment
          | DOLLAR NEWLINE* ID NEWLINE* expr      # patternAssignment
        // | property_assign                     # propAssign
          ;

// таблица с операторами в порядке выполнения
// http://www.difranco.net/compsci/C_Operator_Precedence_Table.htm

expr: function_call                         # functionCall
    | expr NEWLINE? DOT NEWLINE? expr       # dotCall
    | COLON NEWLINE* closure                # closureExpression
    | expr NEWLINE* DCOLON NEWLINE* closure # methodCall
    | LP NEWLINE* expr NEWLINE* RP          # parenthesisedExpression
    | (PLUS|MINUS) expr                     # unaryPlusMinus
    | (NOT|TILDE) expr                      # logicalAndBinaryNegation
    | expr NEWLINE? (MUL|DIV|MOD) NEWLINE? expr     # mulDivModExpression
    | expr NEWLINE? (PLUS|MINUS) NEWLINE? expr      # sumExpression
    | expr NEWLINE? (LSHIFT|RSHIFT) NEWLINE? expr   # binaryShift
    | expr NEWLINE? (LESS|LESSEQ|GREATER|GREATEREQ) NEWLINE? expr # lessGreaterExpression
    | expr NEWLINE? (EQUAL|NOTEQUAL) NEWLINE? expr # equalityExpsression
    | expr NEWLINE? AND NEWLINE? expr              # binaryAndExpression
    | expr NEWLINE? XOR NEWLINE? expr              # binaryXorExpression
    | expr NEWLINE? OR NEWLINE? expr               # binaryOrExpression
    | expr NEWLINE? BOOL_AND NEWLINE? expr         # booleanAndExpression
    | expr NEWLINE? BOOL_OR NEWLINE? expr          # booleanOrExpression
    | ID NEWLINE? (MULASSIGN
        |DIVASSIGN
        |MODASSIGN
        |PLUSASSIGN
        |MINUSASSIGN
        |LSHIFTASSIGN
        |RSHIFTASSIGN
        |ANDASSIGN
        |XORASSIGN
        |ORASSIGN) NEWLINE? expr                    # exprThenAssign
    | RANGE                                         # rangeCall
    | NUMBER                                        # numberCall
    | BOOL                                          # booleanCall
    | string                                        # stringCall
    | ID                                            # idCall
    | expr (LSP NEWLINE* expr NEWLINE* RSP)+        # subscriptCall
    | dictionary                                    # dictionaryCall
    | array                                         # arrayCall
    | pattern_expr                                  # patternExpression
    | property_assign                               # propertyAssign
    ;

string: QUOTE (ANYTHING|L_CURLY NEWLINE* expr NEWLINE* R_CURLY)*? OUT_QUOTE ;

// functions-related
closure       : LP NEWLINE* func_params* NEWLINE* RP
                NEWLINE* (NEWLINE? stat NEWLINE?)+ NEWLINE*
                SEMICOLON                                   ;

function_call : ID LP (NEWLINE* expr NEWLINE*)* RP          ;

// collections-related
array           : LSP NEWLINE* expr (NEWLINE* expr)* NEWLINE* RSP ;
dictionary      : LSP NEWLINE* 
                  (NEWLINE* key_value_pair NEWLINE*)+ 
                  NEWLINE* RSP                                    ;
key_value_pair  : ID NEWLINE* COLON NEWLINE* expr                 ;
property_assign : (ID | pattern) dictionary                       ;

pattern_expr: pattern | bar | EVENT | events_group ;

//
// PATTERN
//

// !!! все PAT_WS важны

pattern: bar+ PAT_WS? PAT_END ;

// bar-related
bar                 : PAT_WS?
                      (VL|BARLINE|bar_property_assign) 
                      PAT_WS? 
                      (events_sequence|multibar_func_call+)   ;

bar_property_assign : (VL|BARLINE) PAT_WS? dictionary PAT_WS? ;

multibar_func_call  : PAT_WS?
                      PAT_L_CURLY PAT_WS? 
                      bar+ PAT_WS? PAT_COLON NEWLINE*
                      expr NEWLINE* (NEWLINE* expr)* NEWLINE*
                      R_CURLY PAT_WS?                         ;

events_sequence     : PAT_WS? events_group PAT_WS?
                      (PAT_WS? PAT_WS? events_group)* PAT_WS? ;

// events group
events_group         : PAT_WS? (
                          EVENT
                          |event_property_assign
                          |nested_events_group
                          |pat_func_call
                        )* 
                        chord
                        (
                          EVENT
                          |event_property_assign
                          |nested_events_group
                          |pat_func_call
                        )*
                     | PAT_WS? (
                          EVENT
                          |event_property_assign
                          |chord
                          |pat_func_call
                        )*
                        nested_events_group 
                        (
                          EVENT
                          |event_property_assign
                          |chord
                          |pat_func_call
                        )*
                     | PAT_WS? (
                          EVENT
                          |event_property_assign
                          |chord
                          |nested_events_group
                        )* 
                        pat_func_call 
                        (
                          EVENT
                          |event_property_assign
                          |chord
                          |nested_events_group
                        )*
                     | (EVENT|event_property_assign)+            ;

chord                : PAT_L_ANGL_BRCKT PAT_WS?
                         events_group 
                         (PAT_WS? events_group)* 
                         PAT_WS? PAT_R_ANGL_BRCKT                ;

nested_events_group  : LP PAT_WS?
                          events_group
                          (PAT_WS? events_group)* PAT_WS?
                       RP                                        ;

event_property_assign : (EVENT|chord) dictionary                 ;

pat_func_call         : PAT_WS? PAT_L_CURLY PAT_WS?
                          (events_group|events_sequence) PAT_WS? 
                          PAT_COLON NEWLINE* 
                          expr NEWLINE* 
                          (NEWLINE* expr)* NEWLINE* 
                        R_CURLY PAT_WS?                          ;
