//
//  CollyTests.swift
//  CollyTests
//
//  Created by Ales Tsurko on 15.10.16.
//  Copyright © 2016 Aliaksandr Tsurko. All rights reserved.
//

import XCTest
@testable import Colly
import Antlr4

class CollyTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
	
	func testVariableAssignment() {
		var str = "a = 2"
		
		let inputStream = ANTLRInputStream(str)
		let lexer = CollyLexer(inputStream)
		let tokens = CommonTokenStream(lexer)
		
		let parser = try! CollyParser(tokens)
		let tree = try! parser.score()
		
		let walker = ParseTreeWalker()
		let listener = Colly()
		try! walker.walk(listener, tree)
	}
	
	func testNumber() {
		var str = "5\n"
		str += "-5\n"
		str += "-.1416\n"
		
		let inputStream = ANTLRInputStream(str)
		let lexer = CollyLexer(inputStream)
		let tokens = CommonTokenStream(lexer)
		
		let parser = try! CollyParser(tokens)
		let tree = try! parser.score()
		
		let walker = ParseTreeWalker()
		let listener = Colly()
		try! walker.walk(listener, tree)
	}
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
